-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 06, 2021 at 10:06 AM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.4.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbsiakad`
--

-- --------------------------------------------------------

--
-- Table structure for table `detail_nilai`
--

CREATE TABLE `detail_nilai` (
  `id_detailNilai` int(10) NOT NULL,
  `id_nilai` int(10) NOT NULL,
  `Tugas1` int(3) NOT NULL,
  `Tugas2` int(3) NOT NULL,
  `UH1` int(3) NOT NULL,
  `UH2` int(3) NOT NULL,
  `UTS` int(3) NOT NULL,
  `UAS` int(3) NOT NULL,
  `nilai_ekskul` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `ekskul`
--

CREATE TABLE `ekskul` (
  `id_ekskul` int(10) NOT NULL,
  `nama_ekskul` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ekskul`
--

INSERT INTO `ekskul` (`id_ekskul`, `nama_ekskul`) VALUES
(3, ' PKS'),
(4, ' PMR'),
(5, ' PRAMUKA'),
(1, 'PASKIBRA'),
(2, 'ROHIS');

-- --------------------------------------------------------

--
-- Table structure for table `guru`
--

CREATE TABLE `guru` (
  `id_guru` int(10) NOT NULL,
  `id_mapel` int(10) NOT NULL,
  `id_user` int(10) NOT NULL,
  `nama` varchar(40) NOT NULL,
  `jk` enum('L','P') NOT NULL,
  `jabatan` varchar(15) NOT NULL,
  `no_hp` char(13) NOT NULL,
  `password` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `guru`
--

INSERT INTO `guru` (`id_guru`, `id_mapel`, `id_user`, `nama`, `jk`, `jabatan`, `no_hp`, `password`) VALUES
(3200001, 1, 3, 'Drs. Abdul', 'L', 'Guru Mapel', '081911980003', '4563'),
(3200002, 2, 3, 'Yuniarti, S.Pd.', 'P', 'Guru Mapel', '085225260000', '12366'),
(3210004, 3, 3, 'Sultan Salman, S.Pd', 'L', 'Guru Mapel', '082324787146', '2323');

-- --------------------------------------------------------

--
-- Table structure for table `jadwal pelajaran`
--

CREATE TABLE `jadwal pelajaran` (
  `id_jadwal` int(10) NOT NULL,
  `id_kelas` int(10) NOT NULL,
  `id_guru` int(10) NOT NULL,
  `hari` enum('SENIN','SELASA','RABU','KAMIS','JUMAT') NOT NULL,
  `jam_mulai` time NOT NULL,
  `jam_selesai` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `jadwal pelajaran`
--

INSERT INTO `jadwal pelajaran` (`id_jadwal`, `id_kelas`, `id_guru`, `hari`, `jam_mulai`, `jam_selesai`) VALUES
(1, 1, 3200001, 'SENIN', '07:00:00', '09:00:00'),
(2, 2, 3200001, 'SELASA', '09:00:00', '11:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `kelas`
--

CREATE TABLE `kelas` (
  `id_kelas` int(10) NOT NULL,
  `kelas` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kelas`
--

INSERT INTO `kelas` (`id_kelas`, `kelas`) VALUES
(1, 'X IPA 1'),
(2, 'X IPA 2'),
(5, 'X IPS 1'),
(3, 'XI IPA 1'),
(4, 'XI IPA 2'),
(6, 'XI IPS 1');

-- --------------------------------------------------------

--
-- Table structure for table `mapel`
--

CREATE TABLE `mapel` (
  `id_mapel` int(10) NOT NULL,
  `nama_mapel` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mapel`
--

INSERT INTO `mapel` (`id_mapel`, `nama_mapel`) VALUES
(5, ' BAHASA INGGRIS'),
(4, 'BAHASA INDONESIA'),
(2, 'BAHASA JAWA'),
(1, 'MATEMATIKA'),
(3, 'OLAHRAGA');

-- --------------------------------------------------------

--
-- Table structure for table `nilai`
--

CREATE TABLE `nilai` (
  `id_nilai` int(10) NOT NULL,
  `id_guru` int(10) NOT NULL,
  `nis` int(10) NOT NULL,
  `id_mapel` int(10) NOT NULL,
  `semester` int(2) NOT NULL,
  `tahun` int(4) NOT NULL,
  `nilai` double NOT NULL,
  `keterangan` char(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `presensi`
--

CREATE TABLE `presensi` (
  `id_presensi` int(10) NOT NULL,
  `nis` int(10) NOT NULL,
  `tanggal` date NOT NULL,
  `keterangan` enum('H','S','I','A') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `siswa`
--

CREATE TABLE `siswa` (
  `nis` int(10) NOT NULL,
  `id_user` int(10) NOT NULL,
  `id_kelas` int(10) NOT NULL,
  `id_ekskul` int(10) NOT NULL,
  `password` varchar(10) NOT NULL,
  `nama_siswa` varchar(40) NOT NULL,
  `jk` enum('L','P') NOT NULL,
  `alamat` text NOT NULL,
  `ttl` date NOT NULL,
  `nama_ortu` varchar(40) NOT NULL,
  `alamat_ortu` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `siswa`
--

INSERT INTO `siswa` (`nis`, `id_user`, `id_kelas`, `id_ekskul`, `password`, `nama_siswa`, `jk`, `alamat`, `ttl`, `nama_ortu`, `alamat_ortu`) VALUES
(2010001, 2, 1, 1, '1235', 'Khiftian Aji Pamungkas', 'L', 'Slawi', '2001-01-02', 'Aliyah', 'Slawi'),
(2020002, 2, 2, 2, '123', 'Bambang Priaji', 'L', 'Margasari', '2003-08-14', 'Paijo', 'Margasari'),
(2030003, 2, 3, 2, '202', 'Agnes Putri Erika', 'P', 'Slawi Kulon', '2004-08-08', 'Sri Wahyuni', 'Slawi Kulon'),
(2110004, 2, 1, 1, '457814', 'Muhammad Adi Saputra', 'L', 'Sleman', '2004-05-05', 'Darjo Setiawan', 'Sleman'),
(2110005, 2, 1, 1, '1313', 'Dendi Setiawan', 'L', 'Depok', '2003-06-07', 'Santoso', 'Depok');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(10) NOT NULL,
  `nama_user` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `nama_user`) VALUES
(1, 'admin'),
(2, 'siswa'),
(3, 'guru');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `detail_nilai`
--
ALTER TABLE `detail_nilai`
  ADD PRIMARY KEY (`id_detailNilai`),
  ADD UNIQUE KEY `id_nilai` (`id_nilai`);

--
-- Indexes for table `ekskul`
--
ALTER TABLE `ekskul`
  ADD PRIMARY KEY (`id_ekskul`),
  ADD UNIQUE KEY `nama_ekskul` (`nama_ekskul`);

--
-- Indexes for table `guru`
--
ALTER TABLE `guru`
  ADD PRIMARY KEY (`id_guru`),
  ADD KEY `fk_mapel` (`id_mapel`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `jadwal pelajaran`
--
ALTER TABLE `jadwal pelajaran`
  ADD PRIMARY KEY (`id_jadwal`),
  ADD KEY `id_guru` (`id_guru`),
  ADD KEY `id_kelas` (`id_kelas`);

--
-- Indexes for table `kelas`
--
ALTER TABLE `kelas`
  ADD PRIMARY KEY (`id_kelas`),
  ADD UNIQUE KEY `kelas` (`kelas`);

--
-- Indexes for table `mapel`
--
ALTER TABLE `mapel`
  ADD PRIMARY KEY (`id_mapel`),
  ADD UNIQUE KEY `nama_mapel` (`nama_mapel`);

--
-- Indexes for table `nilai`
--
ALTER TABLE `nilai`
  ADD PRIMARY KEY (`id_nilai`),
  ADD KEY `fk_guru` (`id_guru`),
  ADD KEY `fk_siswa` (`nis`),
  ADD KEY `id_mapel` (`id_mapel`);

--
-- Indexes for table `presensi`
--
ALTER TABLE `presensi`
  ADD PRIMARY KEY (`id_presensi`),
  ADD KEY `fk_nis` (`nis`);

--
-- Indexes for table `siswa`
--
ALTER TABLE `siswa`
  ADD PRIMARY KEY (`nis`),
  ADD KEY `fk_user` (`id_user`),
  ADD KEY `fk_kelas` (`id_kelas`),
  ADD KEY `fk_ekskul` (`id_ekskul`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `jadwal pelajaran`
--
ALTER TABLE `jadwal pelajaran`
  MODIFY `id_jadwal` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `nilai`
--
ALTER TABLE `nilai`
  MODIFY `id_nilai` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `presensi`
--
ALTER TABLE `presensi`
  MODIFY `id_presensi` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `detail_nilai`
--
ALTER TABLE `detail_nilai`
  ADD CONSTRAINT `detail_nilai_ibfk_1` FOREIGN KEY (`id_nilai`) REFERENCES `nilai` (`id_nilai`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `guru`
--
ALTER TABLE `guru`
  ADD CONSTRAINT `fk_mapel` FOREIGN KEY (`id_mapel`) REFERENCES `mapel` (`id_mapel`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `jadwal pelajaran`
--
ALTER TABLE `jadwal pelajaran`
  ADD CONSTRAINT `jadwal pelajaran_ibfk_1` FOREIGN KEY (`id_guru`) REFERENCES `guru` (`id_guru`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `jadwal pelajaran_ibfk_2` FOREIGN KEY (`id_kelas`) REFERENCES `kelas` (`id_kelas`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `nilai`
--
ALTER TABLE `nilai`
  ADD CONSTRAINT `fk_guru` FOREIGN KEY (`id_guru`) REFERENCES `guru` (`id_guru`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_siswa` FOREIGN KEY (`nis`) REFERENCES `siswa` (`nis`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `nilai_ibfk_1` FOREIGN KEY (`id_mapel`) REFERENCES `mapel` (`id_mapel`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `presensi`
--
ALTER TABLE `presensi`
  ADD CONSTRAINT `fk_nis` FOREIGN KEY (`nis`) REFERENCES `siswa` (`nis`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `siswa`
--
ALTER TABLE `siswa`
  ADD CONSTRAINT `fk_ekskul` FOREIGN KEY (`id_ekskul`) REFERENCES `ekskul` (`id_ekskul`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_kelas` FOREIGN KEY (`id_kelas`) REFERENCES `kelas` (`id_kelas`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_user` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
