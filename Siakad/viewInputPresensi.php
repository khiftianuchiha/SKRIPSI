<?php include ('header.php');?>
<?php require_once ('controller/crudPresensi.php');?>
<?php require_once ('controller/crudKelas.php');?>
<?php require_once ('controller/crudGuru.php');?>
<?php
 if(isset($_SESSION['id_user'])){
     if($_SESSION['id_user']!=1){
        header("Location: 404.php");
     }
 } 


    if(isset($_POST['kelas']) ){
        $id_kelas = $_POST['kelas'];
        $dataKelas = cariKelas($id_kelas);
        $judul = $dataKelas[0]['kelas'];
    }elseif (isset($_GET['kelas'])){
        $id_kelas = $_GET['kelas'];
        $dataKelas = cariKelas($id_kelas);
        $judul = $dataKelas[0]['kelas'];
        
    }else{
      header("Location: viewPresensi.php" );
    }
  
  if (isset($_GET['insert']) || isset($_GET['delete'])){
    $kata = "";
    $x = 0;
    $y = 0;
    if(isset($_GET['insert'])){
      $kata = "Ditambah/Diubah";
      $x = 1;
      $y = 1;
    }elseif(isset($_GET['delete'])){
      $kata = "Dihapus";
      $x = 1;
      $y = 1;
    }else{
      $kata = "";
    } 
  
      if($x==0 || $y==0){
      ?>
      <div class="alert alert-danger" role="alert">
        <strong>Gagal!</strong> Data Gagal <?php echo $kata; ?> Silakan Cek Kembali
      </div>
  <?php
      }elseif($x==1 || $y==1){
  ?>
      
    <div class="alert alert-success" role="alert">
        <strong>Sukses!</strong> Data Berhasil <?php echo $kata; ?>
      </div>
  
  <?php
      }else{
      header("Location: 404.php");
    }
    }
  ?>
  <div class="container" style="margin-top:2%">
  <h3 style="text-align: center">Input Presensi Siswa</h3>
  <form action="controller/insertPresensi.php" method="post">
  <h3 align="center">Kelas <?php echo $judul ?></h3>
  <input type="hidden" class="form-control" name="kelas" id="#" value="<?php echo $id_kelas?>">

  <br/>
    <div class="form-group row">
      <div class="col-sm-1">No.</div>
      <div class="col-sm-2">NIS</div>
      <div class="col-sm-3">NAMA</div>
      <div class="col-sm-1">HADIR</div>
      <div class="col-sm-1">SAKIT</div>
      <div class="col-sm-1">IJIN</div>
      <div class="col-sm-1">ALPHA</div>
      
    </div>
    
  
  <?php
        $sql = "SELECT * FROM `presensi` join siswa ON presensi.nis = siswa.nis join kelas on kelas.id_kelas = siswa.id_kelas WHERE siswa.id_kelas = $id_kelas";
        $data = bacaPresensiJoinSiswa($sql);
        $no=1;
        $a=0;
        if($data != null){
            foreach($data as $baris){
                $nama = $baris['nama_siswa'];
                $nis = $baris['nis'];
                $hadir = $baris['hadir'];
                $sakit = $baris['sakit'];
                $ijin = $baris['ijin'];
                $alpha = $baris['alpha'];
                $id_presensi = $baris['id_presensi'];

            
                ?>
                <div class="form-group row">
                    <input type="hidden" class="form-control" name="nis[<?php echo $a?>]" id="#" value="<?php echo $nis?>">
                    <input type="hidden" class="form-control" name="id_presensi[<?php echo $a?>]" id="#" value="<?php echo $id_presensi?>">
                    <div class="col-sm-1"><?php echo $no;?> </div>
                    <div class="col-sm-2"><?php echo $nis;?> </div>
                    <div class="col-sm-3"><?php echo $nama;?> </div>
                    <div class="col-sm-1"><input type="text" class="form-control" name="hadir[<?php echo $a?>]" id="#" value="<?php echo $hadir ?>"></div>
                    <div class="col-sm-1"><input type="text" class="form-control" name="sakit[<?php echo $a?>]" id="#" value="<?php echo $sakit ?>"></div>
                    <div class="col-sm-1"><input type="text" class="form-control" name="ijin[<?php echo $a?>]" id="#" value="<?php echo $ijin ?>"></div>
                    <div class="col-sm-1"><input type="text" class="form-control" name="alpha[<?php echo $a?>]" id="#" value="<?php echo $alpha ?>"></div>
                </div>
                <?php
                $a++;
                $no++;
            }
        }
  ?>
    <div class="form-group row">
      <div class="col-sm-10">
          <button type="submit" name="Simpan" class="btn btn-primary">Simpan</button>
      </div>
    </div>
  </form>
  </div>
  
<?php include 'footer.php' ?>
<script>

</script>