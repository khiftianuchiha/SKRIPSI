<?php include 'header.php' ?>
<?php require_once ('controller/crudKelas.php');?>
<?php require_once ('controller/crudGuru.php');?>
<?php require_once ('controller/crudMapel.php');?>
<?php
 if(isset($_SESSION['id_user'])){
     if($_SESSION['id_user']!=1){
        header("Location: 404.php");
     }
 } 

if(isset($_SESSION['id_guru'])){
  $id_guru = $_SESSION['id_guru'];
  $data = cariGuru($id_guru);
  if($data !=null){
    $id_guru = $data[0]['id_guru'];
    $nama_guru = $data[0]['nama'];
    $id_mapel = $data[0]['id_mapel'];
  }
}else{
  if(isset($_POST['id_guru'])){
    $data = cariGuru($_POST['id_guru']);
    if($data !=null){
      $id_guru = $data[0]['id_guru'];
      $nama_guru = $data[0]['nama'];
      $id_mapel = $data[0]['id_mapel'];
      $_SESSION["id_guru"] = $id_guru;
    }else{
      header("Location: home.php");
    }
  }
}
  
?>

  <div class="container" style="margin-top:2%">
  <h3 style="text-align: center">Input Nilai Siswa</h3>
  <form action="controller/insertNilai.php" method="post">
    <div class="form-group row">
      <label for="id_guru" class="col-sm-2 col-form-label">ID Guru</label>
      <div class="col-sm-2">
        <input type="text" class="form-control" name="id_guru" value="<?php echo "$id_guru"?>" id="id_guru" readOnly>
      </div>
    </div>
    <div class="form-group row">
      <label for="nama_guru" class="col-sm-2 col-form-label"> Nama Guru</label>
      <div class="col-sm-3">
        <input type="text" class="form-control" name="nama_guru" id="nama_guru" value="<?php echo "$nama_guru"?>" readOnly>
      </div>
    </div>
    <div class="form-group row">
      <div class="col-sm-2">Semester</div>
        <div class="col-sm-2">
          <select class="custom-select" name="semester" id="semester">
            <option selected>-Pilih-</option>
            <option value="1">Semester Ganjil </option>
            <option value="2">Semester Genap </option>
          </select>
        </div>
    </div>
    <div class="form-group row">
      <div class="col-sm-2">Kelas</div>
        <div class="col-sm-2">
          <select class="custom-select" name="kelas" id="kelas">
          <?php
            $sql = "SELECT * FROM `kelas` join `jadwal pelajaran` on kelas.id_kelas = `jadwal pelajaran`.id_kelas where `jadwal pelajaran`.id_guru = $id_guru";
            $data = bacaKelasJoinJadwal($sql);
            if($data != null){
          ?>
              <option selected value="0">-Pilih Kelas-</option>
          <?php
              foreach ($data as $baris){
                $id = $baris['id_kelas'];
                $nama_kelas = $baris['kelas'];
          ?>             
              <option value="<?php echo $id ?> "><?php echo $nama_kelas ?></option>
          <?php
              }
            }else{
          ?>
              <option selected value="0">-Pilih Kelas-</option>
              <option value=" ">Tidak Ada Data</option>
              <?php
            }
          ?>
          </select>
        </div>
    </div>
    <div class="form-group row">
      <div class="col-sm-2">Mata Pelajaran</div>
      <?php 
      $data = cariMapel($id_mapel);
      $mapel = $data[0]['nama_mapel'];
      $id_mapel = $data[0]['id_mapel'];
      ?>
        <div class="col-sm-2">
        <input type="text" class="form-control" name="mapel" id="mapel" value="<?php echo "$mapel"?>" readOnly> 
        <input type="hidden" class="form-control" name="id_mapel" id="" value="<?php echo "$id_mapel"?>"> 
        </div>
    </div>

    <br><hr>
    <div class="form-group row">
      <div class="col-sm-1">No.</div>
      <div class="col-sm-3">Nama</div>
      <div class="col-sm-1">UH 1</div>
      <div class="col-sm-1">UH 2</div>
      <div class="col-sm-1">Tugas 1</div>
      <div class="col-sm-1">Tugas 2</div>
      <div class="col-sm-1">UTS</div>
      <div class="col-sm-1">UAS</div>
      <div class="col-sm-1">Ekskul</div>
    </div>
    <div id="formNilai"></div>

    <div class="form-group row">
      <div class="col-sm-10">
          <button type="submit" name="Simpan" class="btn btn-primary">Simpan</button>
      </div>
    </div>
  </div>
  </form>
  </div>
<?php include 'footer.php' ?>
<script>
  document.getElementById("kelas").onchange = function() {myFunction()};

function myFunction() {
  var x = document.getElementById("kelas");
  var y = document.getElementById("semester");
  console.log(x.value);
  console.log(y.value);
  var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        document.getElementById("formNilai").innerHTML = this.responseText;
      }
    }          
    xhttp.open("GET", "formNilai.php?id="+x.value+"&smt="+y.value, true);
    xhttp.send()
}
</script>