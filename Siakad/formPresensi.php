<?php require_once ('controller/crudPresensi.php');?>
<?php require_once ('controller/crudKelas.php');?>

<?php 
    if(isset($_GET['id'])){
        $id_kelas = $_GET['id'];
        $dataKelas = cariKelas($id_kelas);
        $judul = $dataKelas[0]['kelas'];
    }else{
        header("Location: viewInputPresensi.php" );
    }
?>
<h3 align="center">Kelas <?php echo $judul ?></h3>
</br>
<table class="table table-striped table-bordered table-md" cellspacing="0" width="100%" id="tabelSiswa" >
	<thead class="thead-light" style="text-align:center;vertical-align:middle;">
		<tr >
			<th scope="col" rowspan="2" style="text-align:center;vertical-align:middle;">No</th>
			<th scope="col" rowspan="2" style="text-align:center;vertical-align:middle;">NIS</th>                                           
			<th scope="col" rowspan="2" style="text-align:center;vertical-align:middle;">NAMA</th>                                           
			<th scope="col" colspan="4" style="text-align:center;vertical-align:middle;">KETERANGAN</th>                                                                                   
			<th scope="col" rowspan="2" style="text-align:center;vertical-align:middle;">TOTAL </th>                                                                                                                            
		</tr>
        <tr >
			                                           
			<th scope="col" >HADIR</th>                                           
			<th scope="col" >SAKIT</th>                                           
			<th scope="col" >IJIN</th>                                           
			<th scope="col" >ALPHA</th>                                                                                                                                                                    
		</tr>
	</thead>
	<tbody>
	
		<?php 
			$sql = "SELECT * FROM `presensi` join siswa ON presensi.nis = siswa.nis join kelas on kelas.id_kelas = siswa.id_kelas WHERE siswa.id_kelas = $id_kelas";
			$data = bacaPresensiJoinSiswa($sql);
			$no=1;
			if($data != null){
				foreach($data as $baris){
					$nis = $baris['nis'];
					$nama_siswa = $baris['nama_siswa'];
					$sakit = $baris['sakit'];
					$ijin = $baris['ijin'];
					$alpha = $baris['alpha'];
					$hadir = $baris['hadir'];

					?>
					<tr>
						<td><?php echo $no ?></td>
						<td><?php echo $nis ?></td>
						<td><?php echo $nama_siswa ?></td>
						<td><?php echo $hadir ?></td>
						<td><?php echo $sakit ?></td>
						<td><?php echo $ijin ?></td>
						<td><?php echo $alpha ?></td>
						<td><?php echo $sakit+$ijin+$alpha+$hadir ?></td>
					</tr>
					<?php
					$no++;
				}
			}
		?>
	
    </tbody>
</table>