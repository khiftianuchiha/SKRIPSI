<?php include 'header.php' ?>
<?php
 if(isset($_SESSION['id_user'])){
     if($_SESSION['id_user']!=2){
        header("Location: 404.php");
     }
 } 
?>
    <div class="container" style="margin-top:2%">
        <h2>Selamat Datang Di Siakad SMAN 1 WOJA</h2>
        <div class="row" >  
            <div class="col-md-3" style="margin-top:2%">
                <div class="card-deck">
                    <div class="card">
                        <img class="card-img-top"  src="../image/logo presensi.png" alt="" style="width:200px; height:200px; margin-left:10%;">
                        <div class="card-body">
                            <h4 class="card-title">Presensi</h4>
                            <p class="card-text">Fitur ini digunakan untuk melihat presensi </p>
                            <form action="viewPresensi.php">
                            <button class="btn btn-info" type="submit">Pilih</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3" style="margin-top:2%">
                <div class="card-deck">
                    <div class="card">
                        <img class="card-img-top"  src="../image/icon jadwal.png" alt="" style="width:200px; height:200px; margin-left:10%;">
                        <div class="card-body">
                            <h4 class="card-title">Jadwal Pelajaran</h4>
                            <p class="card-text">Fitur ini digunakan untuk melihat Jadwal Pelajaran </p>
                            <form action="viewJadwal.php">
                            <button class="btn btn-info" type="submit">Pilih</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3" style="margin-top:2%">
                <div class="card-deck">
                    <div class="card">
                        <img class="card-img-top"  src="../image/logo input nilai.png" alt="" style="width:200px; height:200px; margin-left:10%;">
                        <div class="card-body">
                            <h4 class="card-title">Nilai</h4>
                            <p class="card-text">Fitur ini digunakan untuk melihat Nilai </p>
                            <form action="viewNilai.php">
                            <button class="btn btn-info" type="submit">Pilih</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php include 'footer.php' ?>
<script>
    $('#beranda').addClass('active');
</script>