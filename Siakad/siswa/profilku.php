<?php include 'header.php' ?>
<?php include '../controller/cruduser.php' ?>
<?php
 if(isset($_SESSION['id_user'])){
     if($_SESSION['id_user']!=2){
        header("Location: 404.php");
     }
 } 

if(isset($_GET['insert'])){
    if($_GET['insert']==1){
?>
<div class="alert alert-success" role="alert">
    <strong>Sukses!</strong> Data Berhasil Disimpan
</div>
<?php
    }else{
?>
<div class="alert alert-danger" role="alert">
    <strong>Gagal!</strong> Data GAGAL Disimpan
</div>
<?php
    }
}

$sql = "SELECT * FROM `user` join guru ON guru.id_user = user.id_user where id_guru =$user";
$data = bacaUserJoinGuru($sql);
if($data != null){
    $nama = $data[0]['nama'];
    $pwd = $data[0]['password'];
    $jabatan = $data[0]['jabatan'];
    $id_user = $data[0]['id_user'];
    if($data[0]['jk']=='L'){
        $jk ="LAKI-LAKI";
    }else{
        $jk = "PEREMPUAN";
    }
    if($data[0]['jk']=='L'){
        $gmbr = "image/user_male.png";
    }else{
        $gmbr = "image/user_female.png";
    }
}else{
    $sql = "SELECT * FROM `user` join siswa ON siswa.id_user = user.id_user where nis =$user";
    $data = bacaUserJoinSiswa($sql);
    if($data != null){
        $nama = $data[0]['nama_siswa'];
        $pwd = $data[0]['password'];
        $id_user = $data[0]['id_user'];
        $jabatan = "Siswa";
        if($data[0]['jk']=='L'){
            $jk ="LAKI-LAKI";
        }else{
            $jk = "PEREMPUAN";
        }
        if($data[0]['jk']=='L'){
            $gmbr = "image/user_male.png";
        }else{
            $gmbr = "image/user_female.png";
        }
    }else{
        header("Location: index.php?login=0");
    }    
}
?>
<div class="container" style="margin-top:2%">
    <h2 style="text-align:center">Selamat Datang <?php echo $nama ?><br> Di Siakad SMAN 1 WOJA</h2>
    <form method="post" action="../controller/updateProfile.php">
    <div class="row" >
        <div class="col-md-3" style="margin-top:2%">
            <div class="card-deck">
                <div class="card">
                    <img class="card-img-top"  src="../<?php echo $gmbr ?>" alt="" style="width:200px; height:200px; margin-left:10%;">
                    <div class="card-body">
                        <h4 class="card-title" style="text-align:center"><?php echo $jabatan ?></h4>
                    </div>
                </div>
            </div>
        </div>        
        <div class="col-md-9" style="margin-top:2%">
            <div class="card-deck">
                <div class="card">
                    <div class="form-group row" style="padding-top:2%;padding-left:2%">
                        <label for="id" class="col-sm-2 col-form-label">ID</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="id" value="<?php echo $user ?> " id="#" readonly>
                            <input type="hidden" class="form-control" name="id_user" value="<?php echo $id_user ?> " id="#">
                        </div>
                    </div>
                    <div class="form-group row" style="padding-left:2%">
                        <label for="nama" class="col-sm-2 col-form-label">NAMA</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="nama" value="<?php echo $nama ?> " id="#" >
                        </div>
                    </div>
                    <fieldset class="form-group" style="padding-left:2%">
                    <?php $L = "L";$P = "P";?>
                    <div class="row">
                        <legend class="col-form-label col-sm-2 pt-0">Jenis Kelamin</legend>
                        <div class="col-sm-10">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="jk" name="L" value="L" <?php if($data[0]['jk']==$L){echo "checked";} ?>>
                            <label class="form-check-label" for="L">
                            Laki-Laki
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="jk" name="P" value="P" <?php if($data[0]['jk']==$P){echo "checked";} ?>>
                            <label class="form-check-label" for="P">
                            Perempuan
                            </label>
                        </div>
                        </div>
                    </div>
                    </fieldset>
                    <div class="form-group row" style="padding-left:2%">
                        <label label for="pwd" class="col-sm-2 col-form-label">Password</label>
                        <div class="col-sm-3">
                            <input type="password" name="pwd" id="input-pwd" class="form-control" data-toggle="password" value="<?php echo $pwd ?> ">       
                        </div>
                        <div class="col-sm-2">
                            <span toggle="#input-pwd" class="fa fa-fw fa-eye field-icon toggle-password"></span>                            
                        </div>
                    </div>
                    <div class="form-group row" style="padding-left:2%">
                        <div class="col-sm-3">
                            <button type="submit" name="simpan" class="btn btn-primary">Simpan</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>
</div>
<?php include 'footer.php' ?>
<script>
    $('#profilku').addClass('active');

    $('.toggle-password').on('click', function() {
    $(this).toggleClass('fa-eye fa-eye-slash');
    let input = $($(this).attr('toggle'));
    if (input.attr('type') == 'password') {
        input.attr('type', 'text');
    }
    else {
        input.attr('type', 'password');
    }
    });
</script>