<?php include ('header.php');?>
<?php require_once ('../controller/crudJadwalPelajaran.php');?>
<?php require_once ('../controller/crudKelas.php');?>
<?php require_once ('../controller/crudGuru.php');?>
<?php require_once ('../controller/crudSiswa.php');?>
<?php
 if(isset($_SESSION['id_user'])){
     if($_SESSION['id_user']!=2){
        header("Location: 404.php");
     }
 } 
?>

  <div class="container" style="margin-top:2%">
  <h3 style="text-align: center">Nilai</h3>
  <br><hr> 
  <?php
    $cari = cariSiswa($user); 

  ?>
  <br>
  <br>
  <h3 align="center"><?php echo $cari[0]['nama_siswa'] ?></h3>
  </br>
  <table class="table table-striped table-bordered table-md" cellspacing="0" width="100%" id="tabelSiswa">
    <thead class="thead-light">
      <tr>
        <th scope="col" >NO</th>
        <th scope="col" >MATA PELAJARAN </th>                                           
        <th scope="col" >NAMA GURU</th>                                           
        <th scope="col" >NILAI</th>                                                                                                                           
        <th scope="col" >PREDIKAT</th>                                                                                                                           
      </tr>
    </thead>
    <tbody>
    <?php
      $sql = "SELECT * FROM `siswa` join nilai on siswa.nis = nilai.nis join guru on guru.id_guru = nilai.id_guru join mapel on mapel.id_mapel=guru.id_mapel where nilai.nis =$user";
      $data = bacaSiswaJoinNilaiGuruMapel($sql);
      $no=1;
      if($data != null){
        foreach($data as $baris){
          $nama = $baris['nama_siswa'];
          $nama_guru = $baris['nama'];
          $nama_mapel = $baris['nama_mapel'];
          $nilai = $baris['nilai'];
          $keterangan = $baris['keterangan'];
          ?>
          <tr>
            <td><?php echo $no ?></td>
            <td><?php echo $nama_mapel ?></td>
            <td><?php echo $nama_guru ?></td>
            <td><?php echo $nilai ?></td>
            <td><?php echo $keterangan ?></td>
          </tr>
          <?php
          $no++;
        }
      }else{
      ?>
          <tr>
            <td colspan="5">Tidak Ada Data</td>
          </tr>
      <?php
      }
    ?>
    </tbody>
  </table>
  </div>
  <br>
  <br>
  <br>
  <br>
<?php include 'footer.php' ?>
<script>
</script>