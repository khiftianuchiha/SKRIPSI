<?php include ('header.php');?>
<?php require_once ('controller/crudMapel.php');?>
<?php
 if(isset($_SESSION['id_user'])){
     if($_SESSION['id_user']!=1){
        header("Location: 404.php");
     }
 } 

if(isset($_GET['ubah'])){
  $id = $_GET['id_mapel'];
  $data = cariMapel($id);
  $id = $data[0]['id_mapel'];
  $nama_mapel = $data[0]['nama_mapel'];
  $button = "Ubah";
}else{
  $button = "Tambah";
  //id
  $sql = "SELECT * from mapel order by id_mapel DESC LIMIT 1";
  $data = bacaMapel($sql);
  $id = 1;
  if ($data != null){
    $id = $data[0]['id_mapel']+1;
    $nama_mapel = "";
  }else{
    $id = 1;
    $nama_mapel = "";
  }
} 
  
  if (isset($_GET['insert']) || isset($_GET['delete'])){
    $kata = "";
    $x = 0;
    $y = 0;
    if(isset($_GET['insert'])){
      $kata = "Ditambah/Diubah";
      $x = 1;
      $y = 1;
    }elseif(isset($_GET['delete'])){
      $kata = "Dihapus";
      $x = 1;
      $y = 1;
    }else{
      $kata = "";
    } 
  
      if($x==0 || $y==0){
      ?>
      <div class="alert alert-danger" role="alert">
        <strong>Gagal!</strong> Data Gagal <?php echo $kata; ?> Silakan Cek Kembali
      </div>
  <?php
      }elseif($x==1 || $y==1){
  ?>
      
    <div class="alert alert-success" role="alert">
        <strong>Sukses!</strong> Data Berhasil <?php echo $kata; ?>
      </div>
  
  <?php
      }else{
      header("Location: 404.php");
    }
    }
  ?>
  <div class="container" style="margin-top:2%">
  <h3 style="text-align: center">Input Mata Pelajaran</h3>
  <form action="controller/insertMapel.php" method="post">
    <div class="form-group row">
      <label for="id_kelas" class="col-sm-2 col-form-label">ID Mapel</label>
      <div class="col-sm-3">
        <input type="text" class="form-control" name="id_mapel" value="<?php echo $id ?> " id="#" readonly>
      </div>
    </div>
    <div class="form-group row">
      <label for="nama_kelas" class="col-sm-2 col-form-label">Nama Mapel</label>
      <div class="col-sm-3">
        <input type="text" class="form-control" name="nama_mapel" value="<?php echo $nama_mapel ?> ">
      </div>
    </div>
    <div class="form-group row">
      <div class="col-sm-10">
          <button type="submit" name="<?php echo $button ?>" class="btn btn-primary" value="tambah"><?php echo $button ?></button>
      </div>
    </div>
   
    <br><hr>
    <table class="table table-striped table-bordered table-md" cellspacing="0" width="100%" id="tabelSiswa" >
			<thead class="thead-light">
				<tr>
					<th scope="col" >ID Mapel</th>
					<th scope="col" >Nama Mapel</th>                       
					<th scope="col" >Aksi</th>                       
				</tr>
			</thead>
			<tbody>
      <?php
        $data = bacaSemuaMapel();
        if ($data != null){
          foreach($data as $baris){
            $id_mapel = $baris['id_mapel'];
            $nama_mapel = $baris['nama_mapel'];
      ?>
        <tr>
				    <td scope="row"><?php echo $id_mapel ?></td>                  
				    <td><?php echo $nama_mapel ?></td>                
				    <td><a onclick="location.href='viewInputMapel.php?ubah=1&id_mapel=<?php echo $id_mapel?>'" class="btn btn-outline-primary">EDIT</a> || <a class="btn btn-outline-danger" onclick="if(confirm('Apakah anda yakin ingin menghapus data ini ??')){ location.href='controller/deleteMapel.php?id=<?php echo $id_mapel; ?>' }">HAPUS</a></td>                    
			  </tr>
      <?php
          }
      ?>
      <?php
        }else{
          echo "<td colspan='3'>Data Tidak Ada </td>";
        }
      ?>
			</tbody>
		</table>

		<!--ul class="pagination">
			<li class="page-item active"><a class="page-link" href="#">1</a><span class="sr-only">(current)</span></li>
			<li class="page-item">
			<a class="page-link" href="#" readonly>2 </a>
			</li>
			<li class="page-item"><a class="page-link" href="#">3</a></li>
		</ul-->		
    </div>
    

    
  </div>
  </form>
  </div>
<?php include 'footer.php' ?>
<script></script>