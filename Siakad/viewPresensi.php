<?php include ('header.php');?>
<?php require_once ('controller/crudPresensi.php');?>
<?php require_once ('controller/crudKelas.php');?>
<?php require_once ('controller/crudGuru.php');?>
<?php
 if(isset($_SESSION['id_user'])){
     if($_SESSION['id_user']!=1){
        header("Location: 404.php");
     }
 } 

  if (isset($_GET['insert']) || isset($_GET['delete'])){
    $kata = "";
    $x = 0;
    $y = 0;
    if(isset($_GET['insert'])){
      $kata = "Ditambah/Diubah";
      $x = 1;
      $y = 1;
    }elseif(isset($_GET['delete'])){
      $kata = "Dihapus";
      $x = 1;
      $y = 1;
    }else{
      $kata = "";
    } 
  
      if($x==0 || $y==0){
      ?>
      <div class="alert alert-danger" role="alert">
        <strong>Gagal!</strong> Data Gagal <?php echo $kata; ?> Silakan Cek Kembali
      </div>
  <?php
      }elseif($x==1 || $y==1){
  ?>
      
    <div class="alert alert-success" role="alert">
        <strong>Sukses!</strong> Data Berhasil <?php echo $kata; ?>
      </div>
  
  <?php
      }else{
      header("Location: 404.php");
    }
    }
  ?>
  <div class="container" style="margin-top:5%">
  <h3 style="text-align: center;padding-bottom:5%">Daftar Presensi Siswa</h3>
  <form action="viewInputPresensi.php" method="post">
    
    <div class="form-group row">
      <div class="col-sm-2">Kelas</div>
        <div class="col-sm-2">
        <?php
            $data = bacaSemuaKelas();
            if($data != null){
          ?>
          <select class="custom-select" name="kelas" id="kelas">
            <option selected value="0">-Pilih Kelas-</option>
            <?php
              foreach ($data as $baris){
                $id = $baris['id_kelas'];
                $nama_kelas = $baris['kelas'];
          ?>             
              <option value="<?php echo $id ?> "><?php echo $nama_kelas ?></option>
          <?php
              }
            }else{
          ?>
              <option selected value="0">-Pilih Kelas-</option>
              <option value=" ">Tidak Ada Data</option>
              <?php
            }
          ?>      
          </select>
      </div>
    </div>
    
    <div class="form-group row">
      <div class="col-sm-10">
          <button type="submit" name="Tambah" class="btn btn-primary">Tambah</button>
          
      </div>
    </div>

   
    <br > <br ><hr> 
    <div id="formPresensi"></div>
    

		<!--ul class="pagination">
			<li class="page-item active"><a class="page-link" href="#">1</a><span class="sr-only">(current)</span></li>
			<li class="page-item">
			<a class="page-link" href="#" readonly>2 </a>
			</li>
			<li class="page-item"><a class="page-link" href="#">3</a></li>
		</ul-->		
    </div>

    
  </div>
  </form>
  </div>
  
<?php include 'footer.php' ?>
<script>
document.getElementById("kelas").onchange = function() {myFunction()};

function myFunction() {
  var x = document.getElementById("kelas");
  var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        document.getElementById("formPresensi").innerHTML = this.responseText;
      }
    }          
    xhttp.open("GET", "formPresensi.php?id="+x.value, true);
    xhttp.send()
}
</script>