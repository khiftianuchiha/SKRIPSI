<?php require_once ('../controller/crudSiswa.php');?>
<?php require_once ('../controller/crudDetailNilai.php');?>
<?php
if(isset($_GET['id'])){
    $id = $_GET['id'];
    $smt = $_GET['smt'];
    if($id>0){
        $data = cariSiswaDariKelas($id);
        $sql = "SELECT * FROM `detail_nilai` JOIN nilai ON detail_nilai.id_nilai = nilai.id_nilai where nilai.semester = $smt";
        $data_nilai = bacaDetailNilaiJoinNilai($sql);
        if($data_nilai != null){
            $i=0;
            foreach($data_nilai as $row){     
                print("<pre>".print_r($data_nilai,true)."</pre>");           
                $id_detailNilai[$i] = $row['id_detailNilai'];
                $id_nilai[$i] = $row['id_nilai'];
                $tugas1[$i] = $row['Tugas1'];
                $tugas2[$i] = $row['Tugas2'];
                $uh1[$i] = $row['UH1'];
                $uh2[$i] = $row['UH2'];
                $uts[$i] = $row['UTS'];
                $uas[$i] = $row['UAS'];
                $nilai_ekskul[$i] = $row['nilai_ekskul'];
                $i++;                
            }
            $btn = "update";
        }else{
            $tugas1 = 0;
            $tugas2 = 0;
            $uh1 = 0;
            $uh2 = 0;
            $uts = 0;
            $uas = 0;
            $nilai_ekskul = 0;
            $btn = "insert";
        }
        $no = 1;
        $a = 0;
        if($data != null){
            foreach($data as $baris){
                $nama = $baris['nama_siswa'];
                $nis = $baris['nis'];            
                ?>
                <div class="form-group row">
                    <input type="hidden" class="form-control" name="nis[<?php echo $a?>]" id="#" value="<?php echo $nis?>">
                    <input type="hidden" class="form-control" name="id_detailNilai[<?php echo $a?>]" id="#" value="<?php echo $id_detailNilai[$a]?>">
                    <input type="hidden" class="form-control" name="id_nilai[<?php echo $a?>]" id="#" value="<?php echo $id_nilai[$a]?>">

                    <div class="col-sm-1"><?php echo $no;?> </div>
                    <div class="col-sm-3"><?php echo $nama;?> </div>
                    <div class="col-sm-1"><input type="text" class="form-control" name="tugas1[<?php echo $a?>]" id="#" value="<?php echo $tugas1[$a] ?>"></div>
                    <div class="col-sm-1"><input type="text" class="form-control" name="tugas2[<?php echo $a?>]" id="#" value="<?php echo $tugas2[$a] ?>"></div>
                    <div class="col-sm-1"><input type="text" class="form-control" name="uh1[<?php echo $a?>]" id="#" value="<?php echo $uh1[$a] ?>"></div>
                    <div class="col-sm-1"><input type="text" class="form-control" name="uh2[<?php echo $a?>]" id="#" value="<?php echo $uh2[$a] ?>"></div>
                    <div class="col-sm-1"><input type="text" class="form-control" name="uts[<?php echo $a?>]" id="#" value="<?php echo $uts[$a] ?>"></div>
                    <div class="col-sm-1"><input type="text" class="form-control" name="uas[<?php echo $a?>]" id="#" value="<?php echo $uas[$a] ?>"></div>
                    <div class="col-sm-1"><input type="text" class="form-control" name="nilai_ekskul[<?php echo $a?>]" id="#" value="<?php echo $nilai_ekskul[$a] ?>"></div>
                </div>
                <?php
                $a++;
                $no++;
            }
        }
    }else{
    }
}else{
    header("Location: 404.php");
}
?>
