<?php include ('header.php');?>
<?php require_once ('../controller/crudJadwalPelajaran.php');?>
<?php require_once ('../controller/crudKelas.php');?>
<?php require_once ('../controller/crudGuru.php');?>
<?php
 if(isset($_SESSION['id_user'])){
     if($_SESSION['id_user']!=3 ){
        header("Location: 404.php");
     }
 } 
?>

  <div class="container" style="margin-top:2%">
  <h3 style="text-align: center">Jadwal Mata Pelajaran</h3>
  <br><hr> 
  <?php
    $cari = cariJadwalPelajaranGuru($user); 
    $id_kelas = $cari[0]['id_kelas'];
    $dataKelas = cariKelas($id_kelas);
    $judul = $dataKelas[0]['kelas'];
  ?>
  </br>
  <table class="table table-striped table-bordered table-md" cellspacing="0" width="100%" id="tabelSiswa" >
    <thead class="thead-light">
      <tr>
        <th scope="col" ></th>
        <th scope="col" >SENIN</th>                                           
        <th scope="col" >SELASA</th>                                           
        <th scope="col" >RABU</th>                                           
        <th scope="col" >KAMIS</th>                                           
        <th scope="col" >JUMAT</th>                                                                                  
      </tr>
    </thead>
    <tbody>
      <?php
          $sql = "SELECT * FROM `jadwal pelajaran` JOIN guru on guru.id_guru = `jadwal pelajaran`.`id_guru` join kelas on kelas.id_kelas = `jadwal pelajaran`.id_kelas where `jadwal pelajaran`.id_guru = $user order by jam_mulai";
          $data = bacaJadwalPelajaranJoinGuruKelas($sql);
          if ($data != null){
            //print("<pre>".print_r($data,true)."</pre>");
            ?>
            <tr>
              <td>Jam Ke-1</td>
              <td><?php foreach($data as $baris){if($baris['hari']=='SENIN'){ if($baris['jam_mulai']=='07:00:00'){echo $baris['kelas'];}else{ echo " ";}}} ?></td>
              <td><?php foreach($data as $baris){if($baris['hari']=='SELASA'){ if($baris['jam_mulai']=='07:00:00'){echo $baris['kelas'];}else{ echo " ";}}} ?></td>
              <td><?php foreach($data as $baris){if($baris['hari']=='RABU'){ if($baris['jam_mulai']=='07:00:00'){echo $baris['kelas'];}else{ echo " ";}}} ?></td>
              <td><?php foreach($data as $baris){if($baris['hari']=='KAMIS'){ if($baris['jam_mulai']=='07:00:00'){echo $baris['kelas'];}else{ echo " ";}}} ?></td>
              <td><?php foreach($data as $baris){if($baris['hari']=='JUMAT'){ if($baris['jam_mulai']=='07:00:00'){echo $baris['kelas'];}else{ echo " ";}}} ?></td>
            </tr>
            <tr>
              <td>Jam Ke-2</td>
              <td><?php foreach($data as $baris){if($baris['hari']=='SENIN'){ if($baris['jam_mulai']=='09:00:00'){echo $baris['kelas'];}else{ echo " ";}}} ?></td>
              <td><?php foreach($data as $baris){if($baris['hari']=='SELASA'){ if($baris['jam_mulai']=='09:00:00'){echo $baris['kelas'];}else{ echo " ";}}} ?></td>
              <td><?php foreach($data as $baris){if($baris['hari']=='RABU'){ if($baris['jam_mulai']=='09:00:00'){echo $baris['kelas'];}else{ echo " ";}}} ?></td>
              <td><?php foreach($data as $baris){if($baris['hari']=='KAMIS'){ if($baris['jam_mulai']=='09:00:00'){echo $baris['kelas'];}else{ echo " ";}}} ?></td>
              <td><?php foreach($data as $baris){if($baris['hari']=='JUMAT'){ if($baris['jam_mulai']=='09:00:00'){echo $baris['kelas'];}else{ echo " ";}}} ?></td>
            </tr>
            <tr>
              <td>Jam Ke-3</td>
              <td><?php foreach($data as $baris){if($baris['hari']=='SENIN'){ if($baris['jam_mulai']=='12:00:00'){echo $baris['kelas'];}else{ echo " ";}}} ?></td>
              <td><?php foreach($data as $baris){if($baris['hari']=='SELASA'){ if($baris['jam_mulai']=='12:00:00'){echo $baris['kelas'];}else{ echo " ";}}} ?></td>
              <td><?php foreach($data as $baris){if($baris['hari']=='RABU'){ if($baris['jam_mulai']=='12:00:00'){echo $baris['kelas'];}else{ echo " ";}}} ?></td>
              <td><?php foreach($data as $baris){if($baris['hari']=='KAMIS'){ if($baris['jam_mulai']=='12:00:00'){echo $baris['kelas'];}else{ echo " ";}}} ?></td>
              <td><?php foreach($data as $baris){if($baris['hari']=='JUMAT'){ if($baris['jam_mulai']=='12:00:00'){echo $baris['kelas'];}else{ echo " ";}}} ?></td>
            </tr>
            <tr>
              <td>Jam Ke-4</td>
              <td><?php foreach($data as $baris){if($baris['hari']=='SENIN'){ if($baris['jam_mulai']=='14:00:00'){echo $baris['kelas'];}else{ echo " ";}}} ?></td>
              <td><?php foreach($data as $baris){if($baris['hari']=='SELASA'){ if($baris['jam_mulai']=='14:00:00'){echo $baris['kelas'];}else{ echo " ";}}} ?></td>
              <td><?php foreach($data as $baris){if($baris['hari']=='RABU'){ if($baris['jam_mulai']=='14:00:00'){echo $baris['kelas'];}else{ echo " ";}}} ?></td>
              <td><?php foreach($data as $baris){if($baris['hari']=='KAMIS'){ if($baris['jam_mulai']=='14:00:00'){echo $baris['kelas'];}else{ echo " ";}}} ?></td>
              <td><?php foreach($data as $baris){if($baris['hari']=='JUMAT'){ if($baris['jam_mulai']=='14:00:00'){echo $baris['kelas'];}else{ echo " ";}}} ?></td>
            </tr>
            <tr>
              <td>Jam Ke-5</td>
              <td><?php foreach($data as $baris){if($baris['hari']=='SENIN'){ if($baris['jam_mulai']=='16:00:00'){echo $baris['kelas'];}else{ echo " ";}}} ?></td>
              <td><?php foreach($data as $baris){if($baris['hari']=='SELASA'){ if($baris['jam_mulai']=='16:00:00'){echo $baris['kelas'];}else{ echo " ";}}} ?></td>
              <td><?php foreach($data as $baris){if($baris['hari']=='RABU'){ if($baris['jam_mulai']=='16:00:00'){echo $baris['kelas'];}else{ echo " ";}}} ?></td>
              <td><?php foreach($data as $baris){if($baris['hari']=='KAMIS'){ if($baris['jam_mulai']=='16:00:00'){echo $baris['kelas'];}else{ echo " ";}}} ?></td>
              <td><?php foreach($data as $baris){if($baris['hari']=='JUMAT'){ if($baris['jam_mulai']=='16:00:00'){echo $baris['kelas'];}else{ echo " ";}}} ?></td>
            </tr>

            
            <?php
          }else{
            echo "<td colspan='6'>Data Tidak Ada </td>";
          }
        ?>
      </tbody>
  </table>
  </div>
<?php include 'footer.php' ?>
<script>
</script>