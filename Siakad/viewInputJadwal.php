<?php include ('header.php');?>
<?php require_once ('controller/crudJadwalPelajaran.php');?>
<?php require_once ('controller/crudKelas.php');?>
<?php require_once ('controller/crudGuru.php');?>
<?php
 if(isset($_SESSION['id_user'])){
     if($_SESSION['id_user']!=1){
        header("Location: 404.php");
     }
 } 
  
  if (isset($_GET['insert']) || isset($_GET['delete'])){
    $kata = "";
    $x = 0;
    $y = 0;
    if(isset($_GET['insert'])){
      $kata = "Ditambah/Diubah";
      $x = $_GET['insert'];
      $y = 1;
    }elseif(isset($_GET['delete'])){
      $kata = "Dihapus";
      $x = 1;
      $y = 1;
    }else{
      $kata = "";
    } 
  
      if($x==0 || $y==0){
      ?>
      <div class="alert alert-danger" role="alert">
        <strong>Gagal!</strong> Data Gagal <?php echo $kata; ?> Silakan Cek Kembali
      </div>
  <?php
      }elseif($x==1 || $y==1){
  ?>
      
    <div class="alert alert-success" role="alert">
        <strong>Sukses!</strong> Data Berhasil <?php echo $kata; ?>
      </div>
  
  <?php
      }else{
      header("Location: 404.php");
    }
    }
  ?>
  <div class="container" style="margin-top:2%">
  <h3 style="text-align: center">Input Jadwal Mata Pelajaran</h3>
  <form action="controller/insertJadwalMapel.php" method="post">
    
    <div class="form-group row">
      <div class="col-sm-2">Kelas</div>
        <div class="col-sm-2">
        <?php
            $data = bacaSemuaKelas();
            if($data != null){
          ?>
          <select class="custom-select" name="kelas" id="kelas">
            <option selected value="0">-Pilih Kelas-</option>
            <?php
              foreach ($data as $baris){
                $id = $baris['id_kelas'];
                $nama_kelas = $baris['kelas'];
          ?>             
              <option value="<?php echo $id ?> "><?php echo $nama_kelas ?></option>
          <?php
              }
            }else{
          ?>
              <option selected value="0">-Pilih Kelas-</option>
              <option value=" ">Tidak Ada Data</option>
              <?php
            }
          ?>      
          </select>
      </div>
    </div>
    <div class="form-group row">
      <div class="col-sm-2">Nama Guru</div>
        <div class="col-sm-3">
        <?php
            $data = bacaSemuaGuru();
            if($data != null){
          ?>
          <select class="custom-select" name="guru" id="guru">
            <option selected value="0">-Pilih Guru-</option>
            <?php
              foreach ($data as $baris){
                $id = $baris['id_guru'];
                $nama_guru = $baris['nama'];
          ?>             
              <option value="<?php echo $id ?> " label="<?php echo $nama_guru ?>"><?php echo $nama_guru ?></option> 
          <?php
              }
            }else{
          ?>
              <option selected value="0">-Pilih Guru-</option>
              <option value=" ">Tidak Ada Data</option>
              <?php
            }
          ?>      
          </select>
      </div>
    </div>
    <div class="form-group row">
      <div class="col-sm-2">Nama Mapel</div>
        <div class="col-sm-3"><input type="text" class="form-control" id="nama_mapel" readonly></div>       
        <div class="col-sm-3"><input type="hidden" class="form-control" id="id_mapel"  name="id_mapel" readonly></div>       
      </div>

    <div class="form-group row">
      <div class="col-sm-2">Hari</div>
        <div class="col-sm-2">
          <select class="custom-select" name="hari" id="hari">
            <option selected value="0">-Pilih Hari-</option>     
            <option value="SENIN">SENIN</option>     
            <option value="SELASA">SELASA</option>     
            <option value="RABU">RABU</option>     
            <option value="KAMIS">KAMIS</option>     
            <option value="JUMAT">JUMAT</option>     
          </select>
       </div>
      </div>

      <div class="form-group row">
      <div class="col-sm-2">Jam Ke</div>
        <div class="col-sm-2">
          <select class="custom-select" name="jam" id="jam">
            <option selected value="0">-Pilih Jam -</option>
                <option value="1">1</option>                     
                <option value="2">2</option>                     
                <option value="3">3</option>                     
                <option value="4">4</option>                     
                <option value="5">5</option>                     
          </select>
       </div>
      </div>

      <div class="form-group row">
      <div class="col-sm-10">
          <button type="submit" name="Tambah" class="btn btn-primary">Tambah</button>
          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">RESET JADWAL</button>
      </div>
    </div>

   
    <br><hr> 
    <div id="formJadwal"></div>
    

		<!--ul class="pagination">
			<li class="page-item active"><a class="page-link" href="#">1</a><span class="sr-only">(current)</span></li>
			<li class="page-item">
			<a class="page-link" href="#" readonly>2 </a>
			</li>
			<li class="page-item"><a class="page-link" href="#">3</a></li>
		</ul-->		
    </div>

  <!-- Modal -->
  <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">RESET JADWAL PELAJARAN</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          Apakah Anda Yakin untuk mereset Jadwal Pelajaran ?
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">BATAL</button>
          <button type="button" class="btn btn-primary" id="btnReset" name="Reset">OK</button>
        </div>
      </div>
    </div>
  </div>
    

    
  </div>
  </form>
  </div>
<?php include 'footer.php' ?>
<script>
document.getElementById("kelas").onchange = function() {myFunction()};
document.getElementById("guru").onchange = function() {myFunctionMapel()};
var y = document.getElementById("btnReset");

function myFunction() {
  var x = document.getElementById("kelas");
  

  y.value = x.value;
  console.log(y.value);
  console.log(x.value);
  console.log(x.value);
  var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        document.getElementById("formJadwal").innerHTML = this.responseText;
      }
    }          
    xhttp.open("GET", "formJadwal.php?id="+x.value, true);
    xhttp.send()
}

function myFunctionMapel() {
  var x = document.getElementById("guru");
  var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        console.log(this.responseText);
        var myObj = JSON.parse(this.responseText);
        document.getElementById("nama_mapel").value = myObj[0].nama_mapel;
        document.getElementById("id_mapel").value = myObj[0].id_mapel;
      }
    }          
    xhttp.open("GET", "controller/jsonMapel.php?id="+x.value, true);
    xhttp.send()
}

$( "#btnReset" ).click(function() {
  id = y.value;
  url = 'http://localhost/siakad/controller/resetJadwal.php?Reset='+id;
  window.location.href =url;
  
});
</script>