<?php include ('header.php');?>
<?php require_once ('controller/crudKelas.php');?>
<?php
 if(isset($_SESSION['id_user'])){
     if($_SESSION['id_user']!=1){
        header("Location: 404.php");
     }
 } 

if(isset($_GET['ubah'])){
  $id = $_GET['id_kelas'];
  $data = cariKelas($id);
  $id_kelas = $data[0]['id_kelas'];
  $nama_kelas = $data[0]['kelas'];
  $button = "Ubah";
}else{
  $button = "Tambah";
  //id
  $sql = "SELECT * from Kelas order by id_kelas DESC LIMIT 1";
  $data = bacaKelas($sql);
  $id = 1;
  if ($data != null){
    $id = $data[0]['id_kelas']+1;
    $nama_kelas = "";
  }else{
    $id = 1;
    $nama_kelas = "";
  }
}
  

  if (isset($_GET['insert']) || isset($_GET['delete'])){
    $kata = "";
    $x = 0;
    $y = 0;
    if(isset($_GET['insert'])){
      $kata = "Ditambah/Diubah";
      $x = 1;
      $y = 1;
    }elseif(isset($_GET['delete'])){
      $kata = "Dihapus";
      $x = 1;
      $y = 1;
    }else{
      $kata = "";
    } 
  
      if($x==0 || $y==0){
      ?>
      <div class="alert alert-danger" role="alert">
        <strong>Gagal!</strong> Data Gagal <?php echo $kata; ?> Silakan Cek Kembali
      </div>
  <?php
      }elseif($x==1 || $y==1){
  ?>
      
    <div class="alert alert-success" role="alert">
        <strong>Sukses!</strong> Data Berhasil <?php echo $kata; ?>
      </div>
  
  <?php
      }else{
      header("Location: 404.php");
    }
    }
  ?>
  <div class="container" style="margin-top:2%">
  <h3 style="text-align: center">Input Kelas</h3>
  <form action="controller/insertKelas.php" method="post">
    <div class="form-group row">
      <label for="id_kelas" class="col-sm-2 col-form-label">ID Kelas</label>
      <div class="col-sm-3">
        <input type="text" class="form-control" name="id_kelas" value="<?php echo $id ?> " id="#" readonly>
      </div>
    </div>
    <div class="form-group row">
      <label for="nama_kelas" class="col-sm-2 col-form-label" >Nama Kelas</label>
      <div class="col-sm-3">
        <input type="text" class="form-control" name="nama_kelas" value="<?php echo $nama_kelas ?>">
      </div>
    </div>
    <div class="form-group row">
      <div class="col-sm-10">
          <button type="submit" name="<?php echo $button ?>" class="btn btn-primary" value="tambah"><?php echo $button ?></button>
      </div>
    </div>
   
    <br><hr>
    <table class="table table-striped table-bordered table-md" cellspacing="0" width="100%" id="tabelSiswa" >
			<thead class="thead-light">
				<tr>
					<th scope="col" >ID Kelas</th>
					<th scope="col" >Nama Kelas</th>                       
					<th scope="col" >Aksi</th>                       
				</tr>
			</thead>
			<tbody>
      <?php
        $data = bacaSemuaKelas();
        if ($data != null){
          foreach($data as $baris){
            $id_kelas = $baris['id_kelas'];
            $nama_kelas = $baris['kelas'];
      ?>
        <tr>
				    <td scope="row"><?php echo $id_kelas ?></td>                  
				    <td><?php echo $nama_kelas ?></td>                
				    <td><a onclick="location.href='viewInputKelas.php?ubah=1&id_kelas=<?php echo $id_kelas?>'" class="btn btn-outline-primary">EDIT</a> || <a class="btn btn-outline-danger" onclick="if(confirm('Apakah anda yakin ingin menghapus data ini ??')){ location.href='controller/deleteKelas.php?id=<?php echo $id_kelas; ?>' }">HAPUS</a></td>                    
			  </tr>
      <?php
          }
      ?>
      <?php
        }else{
          echo "<td colspan='3'>Data Tidak Ada </td>";
        }
      ?>
			</tbody>
		</table>

		<!--ul class="pagination">
			<li class="page-item active"><a class="page-link" href="#">1</a><span class="sr-only">(current)</span></li>
			<li class="page-item">
			<a class="page-link" href="#" readonly>2 </a>
			</li>
			<li class="page-item"><a class="page-link" href="#">3</a></li>
		</ul-->		
    </div>
    

    
  </div>
  </form>
  </div>
<?php include 'footer.php' ?>
<script></script>