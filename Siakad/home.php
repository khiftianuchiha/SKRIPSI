<?php include 'header.php' ?>
<?php
 if(isset($_SESSION['id_user'])){
     if($_SESSION['id_user']!=1){
        header("Location: 404.php");
     }
 } 
?>
    <div class="container" style="margin-top:2%">
        <h2>Selamat Datang Di Siakad SMAN 1 WOJA</h2>
        <div class="row" >
            <div class="col-md-3" style="margin-top:2%">
                <div class="card-deck">
                    <div class="card">
                        <img class="card-img-top"  src="image/logo Siswa.png" alt="" style="width:200px; height:200px; margin-left:10%;">
                        <div class="card-body">
                            <h4 class="card-title">Siswa</h4>
                            <p class="card-text">Fitur ini digunakan untuk mengelola data siswa </p>
                            <form action="viewSiswa.php">
                            <button class="btn btn-info" type="submit">Pilih</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3" style="margin-top:2%">
                <div class="card-deck">
                    <div class="card">
                        <img class="card-img-top"  src="image/logo Guru.png" alt="" style="width:200px; height:200px; margin-left:10%;">
                        <div class="card-body">
                            <h4 class="card-title">Guru</h4>
                            <p class="card-text">Fitur ini digunakan untuk mengelola data guru </p>
                            <form action="viewGuru.php">
                            <button class="btn btn-info" type="submit">Pilih</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3" style="margin-top:2%">
                <div class="card-deck">
                    <div class="card">
                        <img class="card-img-top"  src="image/logo presensi.png" alt="" style="width:200px; height:200px; margin-left:10%;">
                        <div class="card-body">
                            <h4 class="card-title">Presensi</h4>
                            <p class="card-text">Fitur ini digunakan untuk mengelola data presensi. </p>
                            <form action="viewPresensi.php">
                            <button class="btn btn-info" type="submit">Pilih</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3" style="margin-top:2%">
                <div class="card-deck">
                    <div class="card">
                        <img class="card-img-top"  src="image/icon jadwal.png" alt="" style="width:200px; height:200px; margin-left:10%;">
                        <div class="card-body">
                            <h4 class="card-title">Jadwal Pelajaran</h4>
                            <p class="card-text">Fitur ini digunakan untuk mengelola data jadwal mata pelajaran. </p>
                            <form action="viewInputJadwal.php">
                            <button class="btn btn-info" type="submit">Pilih</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3" style="margin-top:2%">
                <div class="card-deck">
                    <div class="card">
                        <img class="card-img-top"  src="image/logo input nilai.png" alt="" style="width:200px; height:200px; margin-left:10%;">
                        <div class="card-body">
                            <h4 class="card-title">Nilai</h4>
                            <p class="card-text">Fitur ini digunakan untuk mengelola data nilai. </p>
                            <button class="btn btn-info" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">Pilih</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3" style="margin-top:2%">
                <div class="card-deck">
                    <div class="card">
                        <img class="card-img-top"  src="image/logo Ekskul.png" alt="" style="width:200px; height:200px; margin-left:10%;">
                        <div class="card-body">
                            <h4 class="card-title">Ekskul</h4>
                            <p class="card-text">Fitur ini digunakan untuk mengelola data ekskul. </p>
                            <form action="viewInputEkskul.php">
                            <button class="btn btn-info" type="submit">Pilih</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3" style="margin-top:2%">
                <div class="card-deck">
                    <div class="card">
                        <img class="card-img-top"  src="image/logo_kelas.png" alt="" style="width:200px; height:200px; margin-left:10%;">
                        <div class="card-body">
                            <h4 class="card-title">Kelas</h4>
                            <p class="card-text">Fitur ini digunakan untuk mengelola data kelas. </p>
                            <form action="viewInputKelas.php">
                            <button class="btn btn-info" type="submit">Pilih</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3" style="margin-top:2%">
                <div class="card-deck">
                    <div class="card">
                        <img class="card-img-top"  src="image/logo_mapel.png" alt="" style="width:200px; height:200px; margin-left:10%;">
                        <div class="card-body">
                            <h4 class="card-title">Mata Pelajaran</h4>
                            <p class="card-text">Fitur ini digunakan untuk mengelola data mata pelajaran. </p>
                            <form action="viewInputMapel.php">
                            <button class="btn btn-info" type="submit">Pilih</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
    </div>

<!-- Modal Nilai -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Masukan ID Guru</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="viewInputNilai.php" method="POST">
      <div class="modal-body">
        <input type="text" class="form-control" name="id_guru" placeholder="Masukan ID Guru">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button type="Submit" class="btn btn-primary">Masuk</button>
      </div>
    </form>
    </div>
  </div>
</div>

<?php include 'footer.php' ?>
<script>
    $('#beranda').addClass('active');
</script>