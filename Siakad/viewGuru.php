<?php include 'header.php' ?>
<?php require_once ('controller/crudGuru.php');?>
<?php
 if(isset($_SESSION['id_user'])){
     if($_SESSION['id_user']!=1){
        header("Location: 404.php");
     }
 } 

if (isset($_GET['insert']) || isset($_GET['delete'])){
	$kata = "";
	$x = 0;
	$y = 0;
	if(isset($_GET['insert'])){
		$kata = "Ditambah/Disimpan";
		$x = 1;
		$y = 1;
	}elseif(isset($_GET['delete'])){
		$kata = "Dihapus";
		$x = 1;
		$y = 1;
	}else{
		$kata = "";
	} 

    if($x==0 || $y==0){
    ?>
    <div class="alert alert-danger" role="alert">
      <strong>Gagal!</strong> Data Gagal <?php echo $kata; ?> Silakan Cek Kembali
    </div>
<?php
    }elseif($x==1 || $y==1){
?>
    
	<div class="alert alert-success" role="alert">
      <strong>Sukses!</strong> Data Berhasil <?php echo $kata; ?>
    </div>

<?php
    }else{
		header("Location: 404.php");
	}
  }
?>

    <div class="container" style="margin-top:2%">
	<h2 align="center">Data Guru </br> SMA NEGERI 1 WOJA</h2>
        <div class="form-group mx-sm-3 mb-2">
			<div class="row">
				<div class="col-md-3">
					<label for="search" class="sr-only">Search</label>
					<input type="text" name="nis" class="form-control" id="nis" placeholder="Masukan ID Guru" onkeyup="search()">
				</div>
				<div class="col-md-3">
					<form action="viewInputGuru.php"><button class="btn btn-info" >Tambah Guru</button>
				</div>
			</div>
		</div>
		<table class="table" id="tabelSiswa">
			<thead class="thead-light">
				<tr>
					<th scope="col">No</th>
					<th scope="col">ID Guru</th>
					<th scope="col">Nama</th>                       
					<th scope="col">Jenis Kelamin</th>
					<th scope="col">Mapel</th>
					<th scope="col">Jabatan</th>
					<th scope="col">No. Hp</th>
					<th scope="col">Aksi</th>

				</tr>
			</thead>
			<tbody>
			<?php
				$sql = "SELECT * FROM `Guru` JOIN Mapel ON Guru.id_mapel = Mapel.id_mapel";
				$data = bacaGuruJoinMapel($sql);
				$no = 1;
				if($data != null){
					foreach ($data as $baris){
						$id_guru = $baris['id_guru'];
						$nama_guru = $baris['nama'];
						$jk = $baris['jk'];
						$mapel = $baris['nama_mapel'];
						$jabatan = $baris['jabatan'];
						$nohp = $baris['no_hp'];

						if($jk == "L" ){
							$jk = "Laki-laki";
						}else{
							$jk = "Perempuan";
						}
			?>
				<tr>
				    <td scope="row"><?php echo $no ?></td>                  
				    <td ><?php echo $id_guru ?></td>                  
				    <td><?php echo $nama_guru ?></td>                               
				    <td><?php echo $jk ?></td>                
				    <td><?php echo $mapel ?></td>                
				    <td><?php echo $jabatan ?></td>                
				    <td><?php echo $nohp ?></td>                
				    <td><a onclick="location.href='viewUpdateGuru.php?ubah=1&id_guru=<?php echo $id_guru?>'" class="btn btn-outline-primary">EDIT</a> || <a class="btn btn-outline-danger" onclick="if(confirm('Apakah anda yakin ingin menghapus data ini ??')){ location.href='controller/deleteGuru.php?id_guru=<?php echo $id_guru; ?>' }">HAPUS</a></td>                    
			    </tr>
			<?php
					$no++;
					}
				}else{
				echo "<td colspan='9'>Data Tidak Ada </td>";
				}
			?>
			</tbody>
		</table>
		<!--ul class="pagination">
			<li class="page-item active"><a class="page-link" href="#">1</a><span class="sr-only">(current)</span></li>
			<li class="page-item">
			<a class="page-link" href="#">2 </a>
			</li>
			<li class="page-item"><a class="page-link" href="#">3</a></li>
		</ul-->		
    </div>


<?php include 'footer.php' ?>
<script>
    $('#beranda').addClass('active');

    function search() {
        var input, filter, table, tr, td, i, txtValue;
        input = document.getElementById("nis");
        filter = input.value.toUpperCase();
        table = document.getElementById("tabelSiswa");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[0];
            if (td) {
                txtValue = td.textContent || td.innerText;
                if (txtValue.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }
</script>