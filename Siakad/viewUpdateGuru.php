<?php include 'header.php' ?>
<?php require_once ('controller/crudGuru.php');?>
<?php require_once ('controller/crudMapel.php');?>
<?php
 if(isset($_SESSION['id_user'])){
     if($_SESSION['id_user']!=1){
        header("Location: 404.php");
     }
 } 

  if($_GET['ubah']==1){
    $id_guru = $_GET['id_guru'];
    $data = cariGuru($id_guru);
    foreach($data as $baris){
      if($data != null){
        $id_guru = $baris['id_guru'];
        $nama_guru = $baris['nama'];
        $jk = $baris['jk'];
        $id_mapel = $baris['id_mapel'];
        $jabatan = $baris['jabatan'];
        $nohp = $baris['no_hp'];
  
        if($jk == "L" ){
          $jk = "Laki-laki";
        }else{
          $jk = "Perempuan";
        }
  
      }else{
        header("Location: viewGuru.php?ubah=0");
      }
    }

  }else{
    header("Location: ../404.php");
  }

?>

  <div class="container" style="margin-top:2%">
  <h3 style="text-align: center">Ubah Data Guru</h3>
  <form action="controller/updateGuru.php" method="post">
    <div class="form-group row">
      <label for="id_guru" class="col-sm-2 col-form-label">ID Guru</label>
      <div class="col-sm-2">
        <input type="text" class="form-control" name="id_guru" id="id_guru" readonly value="<?php echo $id_guru; ?>">
      </div>
    </div>
    <div class="form-group row">
      <label for="nama" class="col-sm-2 col-form-label">Nama</label>
      <div class="col-sm-5">
        <input type="text" class="form-control" name="nama" placeholder="Masukkan Nama Lengkap" value="<?php echo $nama_guru ?>">
      </div>
    </div>
    <fieldset class="form-group">
    <?php $L = "L";$P = "P";?>
      <div class="row">
        <legend class="col-form-label col-sm-2 pt-0">Jenis Kelamin</legend>
        <div class="col-sm-10">
          <div class="form-check">
            <input class="form-check-input" type="radio" name="JK" name="L" value="L" <?php if($baris['jk']==$L){echo "checked";} ?>>
            <label class="form-check-label" for="L">
            Laki-Laki
            </label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="radio" name="JK" name="P" value="P" <?php if($baris['jk']==$P){echo "checked";} ?>>
            <label class="form-check-label" for="P">
            Perempuan
            </label>
          </div>
        </div>
      </div>
    </fieldset>
    
    <div class="form-group row">
      <label for="jabatan" class="col-sm-2 col-form-label">Jabatan</label>
      <div class="col-sm-5">
        <input type="text" class="form-control" name="jabatan" id="jabatan" placeholder="Masukkan Jabatan Anda Di Sekolah" value="<?php echo $jabatan ?>">
      </div>
    </div>

    <div class="form-group row">
      <div class="col-sm-2">Mata Pelajaran</div>
        <div class="col-sm-2">
        <?php
            $data = bacaSemuaMapel();
            if($data != null){
          ?>
          <select class="custom-select" name="mapel">
            <option selected>-Pilih-</option>
            <?php
              foreach ($data as $baris){
                if($baris['id_mapel']==$id_mapel){
                  $selected = "selected";
                }else{
                  $selected = " ";
                }
                $id = $baris['id_mapel'];
                $nama_mapel = $baris['nama_mapel'];
          ?>             
              <option value="<?php echo $id ?> "  <?php echo $selected ?> ><?php echo $nama_mapel ?></option>
          <?php
              }
            }else{
          ?>
              <option selected>-Pilih Mapel-</option>
              <option value=" ">Tidak Ada Data</option>
              <?php
            }
          ?>      
          </select>
      </div>
    </div>

    <div class="form-group row">
      <label for="nohp" class="col-sm-2 col-form-label">No. HP</label>
      <div class="col-sm-3">
        <input type="number" class="form-control" name="nohp" id="nohp" placeholder="Masukkan Nomor Handphone Anda" value="<?php echo $nohp ?>">
      </div>
    </div>

    <div class="form-group row">
      <label for="pwd" class="col-sm-2 col-form-label">Password</label>
      <div class="col-sm-3">
        <input type="password" class="form-control" name="pwd" id="pwd" placeholder="Masukkan Password">
      </div>
    </div>

    <div class="form-group row">
      <div class="col-sm-10">
          <button type="submit" name="Tambah" class="btn btn-primary">Tambah</button>
          <button type="submit" name="Batal" class="btn btn-primary">Batal</button>
      </div>
    </div>
  </div>
  </form>
  </div>

<?php include 'footer.php' ?>
<script></script>