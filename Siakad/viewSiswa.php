<?php include 'header.php' ?>
<?php require_once ('controller/crudSiswa.php');?>
<?php
 if(isset($_SESSION['id_user'])){
     if($_SESSION['id_user']!=1){
        header("Location: 404.php");
     }
 } 

if (isset($_GET['insert']) || isset($_GET['delete'])){
	$kata = "";
	$x = 0;
	$y = 0;
	if(isset($_GET['insert'])){
		$kata = "Ditambah";
		$x = 1;
		$y = 1;
	}elseif(isset($_GET['delete'])){
		$kata = "Dihapus";
		$x = 1;
		$y = 1;
	}else{
		$kata = "";
	} 

    if($x==0 || $y==0){
    ?>
    <div class="alert alert-danger" role="alert">
      <strong>Gagal!</strong> Data Gagal <?php echo $kata; ?> Silakan Cek Kembali
    </div>
<?php
    }elseif($x==1 || $y==1){
?>
    
	<div class="alert alert-success" role="alert">
      <strong>Sukses!</strong> Data Berhasil <?php echo $kata; ?>
    </div>

<?php
    }else{
		header("Location: 404.php");
	}
  }
?>

    <div class="container" style="margin-top:2%">
        <h2 align="center">Data Siswa </br> SMA NEGERI 1 WOJA</h2>
        <div class="form-group mx-sm-3 mb-2">
			<div class="row">
				<div class="col-md-3">
					<label for="search" class="sr-only">Search</label>
					<input type="text" name="nis" class="form-control" id="nis" placeholder="Masukan NIS untuk Mencari" onkeyup="search()">
				</div>
				<div class="col-md-3">
					<form action="viewInputSiswa.php"><button class="btn btn-info">Tambah Siswa</button>
				</div>
			</div>
		</div>

		<table class="table table-striped table-bordered table-md" cellspacing="0" width="100%" id="tabelSiswa" >
			<thead class="thead-light">
				<tr>
					<th scope="col">No</th>
					<th scope="col">NIS</th>
					<th scope="col">Nama</th>                       
					<th scope="col">Kelas</th>
					<th scope="col">Jenis Kelamin</th>
					<th scope="col">Tanggal Lahir</th>
					<th scope="col">Alamat</th>
					<th scope="col">Nama Orang Tua</th>
					<th scope="col">Aksi</th>

				</tr>
			</thead>
			<tbody>
			<?php
				$sql = "SELECT * FROM `siswa` JOIN kelas ON siswa.id_kelas = kelas.id_kelas";
				$data = bacaSiswaJoinKelas($sql);
				$x = 0;
				$no = 1;
				if($data != null){
					foreach ($data as $baris){
						$nis = $baris['nis'];
						$nama_siswa = $baris['nama_siswa'];
						$kelas = $baris['kelas'];
						$jk = $baris['jk'];
						$ttl = $baris['ttl'];
						$alamat = $baris['alamat'];
						$nama_ortu = $baris['nama_ortu'];

						if($jk == "L" ){
							$jk = "Laki-laki";
						}else{
							$jk = "Perempuan";
						}
			?>
				<tr>
				    <td scope="row"><?php echo $no ?></td>                  
				    <td ><?php echo $nis ?></td>                  
				    <td><?php echo $nama_siswa ?></td>                
				    <td><?php echo $kelas ?></td>                
				    <td><?php echo $jk ?></td>                
				    <td><?php echo $ttl ?></td>                
				    <td><?php echo $alamat ?></td>                
				    <td><?php echo $nama_ortu ?></td>                
				    <td><a onclick="location.href='viewUpdateSiswa.php?ubah=1&nis=<?php echo $nis?>'" class="btn btn-outline-primary">EDIT</a> || <a class="btn btn-outline-danger" onclick="if(confirm('Apakah anda yakin ingin menghapus data ini ??')){ location.href='controller/deleteSiswa.php?nis=<?php echo $nis; ?>' }">HAPUS</a> </td>                    
			    </tr>
			<?php
					$no++;
					$x++;
					}
				}else{
				echo "<td colspan='9'>Data Tidak Ada </td>";
				}
			?>
			</tbody>
		</table>

		<!--ul class="pagination">
			<li class="page-item active"><a class="page-link" href="#">1</a><span class="sr-only">(current)</span></li>
			<li class="page-item">
			<a class="page-link" href="#">2 </a>
			</li>
			<li class="page-item"><a class="page-link" href="#">3</a></li>
		</ul-->		
    </div>

	<!-- Modal Hapus -->
<div class="modal fade" id="modalHapus" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Hapus Data Siswa</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="deleteSiswa.php" method="POST">
      <div class="modal-body">
        <input type="hidden" class="form-control" name="nis" id="nis_modal">
		Apakah Anda Yakin Untuk Menghapus Siswa Dengan NIS <h3 id="modal_nis"></h3>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button type="Submit" class="btn btn-primary">Hapus</button>
      </div>
    </form>
    </div>
  </div>
</div>

<?php include 'footer.php' ?>
<script>
    $('#beranda').addClass('active');

    function search() {
        var input, filter, table, tr, td, i, txtValue;
        input = document.getElementById("nis");
        filter = input.value.toUpperCase();
        table = document.getElementById("tabelSiswa");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[1];
            if (td) {
                txtValue = td.textContent || td.innerText;
                if (txtValue.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
	}

</script>