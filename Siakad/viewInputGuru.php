<?php include 'header.php' ?>
<?php require_once ('controller/crudGuru.php');?>
<?php require_once ('controller/crudMapel.php');?>
<?php
 if(isset($_SESSION['id_user'])){
     if($_SESSION['id_user']!=1){
        header("Location: 404.php");
     }
 } 

  //membuat id guru
  $sql = "SELECT * from Guru order by id_guru DESC LIMIT 1";
  $data = bacaGuru($sql);
  $id = "0001";
  if ($data != null){
      $id = $data[0]['id_guru']+1;
      $id = substr($id,3);
      $id = "3".date("y").$id;
  }else{
      $id = "3".date("y").$id;
  }
  $id_guru = str_replace(' ','',$id);
?>

  <div class="container" style="margin-top:2%">
  <h3 style="text-align: center">Input Guru</h3>
  <form action="controller/insertGuru.php" method="post">
    <div class="form-group row">
      <label for="id_guru" class="col-sm-2 col-form-label">ID Guru</label>
      <div class="col-sm-2">
        <input type="text" class="form-control" name="id_guru" id="id_guru" readonly value="<?php echo $id_guru; ?>">
      </div>
    </div>
    <div class="form-group row">
      <label for="nama" class="col-sm-2 col-form-label">Nama</label>
      <div class="col-sm-5">
        <input type="text" class="form-control" name="nama" placeholder="Masukkan Nama Lengkap">
      </div>
    </div>
    <fieldset class="form-group">
      <div class="row">
        <legend class="col-form-label col-sm-2 pt-0">Jenis Kelamin</legend>
        <div class="col-sm-10">
          <div class="form-check">
            <input class="form-check-input" type="radio" name="JK" name="L" value="L" checked>
            <label class="form-check-label" for="L">
            Laki-Laki
            </label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="radio" name="JK" name="P" value="P">
            <label class="form-check-label" for="P">
            Perempuan
            </label>
          </div>
        </div>
      </div>
    </fieldset>
    
    <div class="form-group row">
      <label for="jabatan" class="col-sm-2 col-form-label">Jabatan</label>
      <div class="col-sm-5">
        <input type="text" class="form-control" name="jabatan" id="jabatan" placeholder="Masukkan Jabatan Anda Di Sekolah">
      </div>
    </div>

    <div class="form-group row">
      <div class="col-sm-2">Mata Pelajaran</div>
        <div class="col-sm-2">
        <?php
            $data = bacaSemuaMapel();
            if($data != null){
          ?>
          <select class="custom-select" name="mapel">
            <option selected>-Pilih-</option>
            <?php
              foreach ($data as $baris){
                $id = $baris['id_mapel'];
                $nama_mapel = $baris['nama_mapel'];
          ?>             
              <option value="<?php echo $id ?> "><?php echo $nama_mapel ?></option>
          <?php
              }
            }else{
          ?>
              <option selected>-Pilih Mapel-</option>
              <option value=" ">Tidak Ada Data</option>
              <?php
            }
          ?>      
          </select>
      </div>
    </div>

    <div class="form-group row">
      <label for="nohp" class="col-sm-2 col-form-label">No. HP</label>
      <div class="col-sm-3">
        <input type="number" class="form-control" name="nohp" id="nohp" placeholder="Masukkan Nomor Handphone Anda">
      </div>
    </div>

    <div class="form-group row">
      <label for="pwd" class="col-sm-2 col-form-label">Password</label>
      <div class="col-sm-3">
        <input type="password" class="form-control" name="pwd" id="pwd" placeholder="Masukkan Password">
      </div>
    </div>

    <div class="form-group row">
      <div class="col-sm-10">
          <button type="submit" name="Tambah" class="btn btn-primary">Tambah</button>
          <button type="submit" name="Batal" class="btn btn-primary">Batal</button>
      </div>
    </div>
  </div>
  </form>
  </div>
<?php include 'footer.php' ?>
<script></script>