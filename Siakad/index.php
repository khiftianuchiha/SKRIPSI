<!doctype html>
<html lang="en">
  <head>
    <title>Siakad SMAN 1 WOJA</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="assets/css/main.css">
  </head>
  <body style="background-color: #d2d6de">
  <?php
    if(isset($_GET['login'])){
      if($_GET['login']==0){
  ?>
      <div class="alert alert-danger" role="alert" style="text-align:center;">
      <strong>Gagal!</strong> LOGIN !!! Silakan Cek Kembali
      </div>
  <?php
      }
    }
  
  ?>
   <div class="login-page">
  <div class="form">
    <div class="login-form">
      <h4 style="text-align:center; font-family:Helvetica Neue;font-weight:1;">Siakad SMAN 1 WOJA</h4>
      </br>
      <input id="user" type="text" placeholder="ID PENGGUNA"/>
      <input id="password" type="password" placeholder="SANDI"/>
      <button id="login" onclick="login()" >MASUK</button>
    </div>
  </div>
</div>
      
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
</body>
</html>
<script>
function login(){
  username = document.getElementById("user");
  password = document.getElementById("password");
  window.location.href = "login.php?username="+username.value+"&password="+password.value;
}

window.setTimeout(function() {
  $(".alert").fadeTo(300, 0).slideUp(300, function(){
  $(this).remove(); 
  });
}, 3000);
</script>