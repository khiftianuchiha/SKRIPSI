<?php include 'header.php' ?>
<?php require_once ('../controller/crudGuru.php');?>
<?php require_once ('../controller/crudSiswa.php');?>
<?php require_once ('../controller/crudJadwalPelajaran.php');?>
<?php require_once ('../controller/crudKelas.php');?>
<?php
 if(isset($_SESSION['id_user'])){
     if($_SESSION['id_user']!=4 ){
        header("Location: 404.php");
     }
 } 
?>
    <div class="container" style="margin-top:2%">
        <h2 style="padding-top:2%; padding-bottom:5%">Selamat Datang Di Siakad SMAN 1 WOJA</h2>
        <nav>
            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                <a class="nav-item nav-link active" id="nav-guru-tab" data-toggle="tab" href="#nav-guru" role="tab" aria-controls="nav-guru" aria-selected="true">Data Guru</a>
                <a class="nav-item nav-link" id="nav-siswa-tab" data-toggle="tab" href="#nav-siswa" role="tab" aria-controls="nav-siswa" aria-selected="false">Data Siswa</a>
                <a class="nav-item nav-link" id="nav-nilai-tab" data-toggle="tab" href="#nav-nilai" role="tab" aria-controls="nav-nilai" aria-selected="false">Nilai</a>
                <a class="nav-item nav-link" id="nav-jadwal-tab" data-toggle="tab" href="#nav-jadwal" role="tab" aria-controls="nav-jadwal" aria-selected="false">Jadwal Pelajaran</a>
            </div>
        </nav>
        <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane fade show active" id="nav-guru" role="tabpanel" aria-labelledby="nav-guru-tab">
            <div class="form-group mx-sm-3 mb-2" style="padding-top:2%;">
                <div class="row">
                    <div class="col-md-3">
                        <label for="search" class="sr-only">Search</label>
                        <input type="text" name="nis" class="form-control" id="idguru" placeholder="Masukan ID Guru" onkeyup="searchGuru()">
                    </div>
                </div>
            </div>
            <table class="table" id="tabelGuru">
                <thead class="thead-light">
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">ID Guru</th>
                        <th scope="col">Nama</th>                       
                        <th scope="col">Jenis Kelamin</th>
                        <th scope="col">Mapel</th>
                        <th scope="col">Jabatan</th>
                        <th scope="col">No. Hp</th>

                    </tr>
                </thead>
                <tbody>
                <?php
                    $sql = "SELECT * FROM `Guru` JOIN Mapel ON Guru.id_mapel = Mapel.id_mapel";
                    $data = bacaGuruJoinMapel($sql);
                    $no = 1;
                    if($data != null){
                        foreach ($data as $baris){
                            $id_guru = $baris['id_guru'];
                            $nama_guru = $baris['nama'];
                            $jk = $baris['jk'];
                            $mapel = $baris['nama_mapel'];
                            $jabatan = $baris['jabatan'];
                            $nohp = $baris['no_hp'];

                            if($jk == "L" ){
                                $jk = "Laki-laki";
                            }else{
                                $jk = "Perempuan";
                            }
                ?>
                    <tr>
                        <td scope="row"><?php echo $no ?></td>                  
                        <td ><?php echo $id_guru ?></td>                  
                        <td><?php echo $nama_guru ?></td>                               
                        <td><?php echo $jk ?></td>                
                        <td><?php echo $mapel ?></td>                
                        <td><?php echo $jabatan ?></td>                
                        <td><?php echo $nohp ?></td>                                  
                    </tr>
                <?php
                        $no++;
                        }
                    }else{
                    echo "<td colspan='9'>Data Tidak Ada </td>";
                    }
                ?>
                </tbody>
            </table>                    
            </div>

            <div class="tab-pane fade" id="nav-siswa" role="tabpanel" aria-labelledby="nav-siswa-tab">
            <div class="form-group mx-sm-3 mb-2" style="padding-top:2%;">
                <div class="row">
                    <div class="col-md-3">
                        <label for="search" class="sr-only">Search</label>
                        <input type="text" name="nis" class="form-control" id="nis" placeholder="Masukan NIS untuk Mencari" onkeyup="search()">
                    </div>
                </div>
            </div>

            <table class="table table-striped table-bordered table-md" cellspacing="0" width="100%" id="tabelSiswa" >
                <thead class="thead-light">
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">NIS</th>
                        <th scope="col">Nama</th>                       
                        <th scope="col">Kelas</th>
                        <th scope="col">Jenis Kelamin</th>
                        <th scope="col">Tanggal Lahir</th>
                        <th scope="col">Alamat</th>
                        <th scope="col">Nama Orang Tua</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                    $sql = "SELECT * FROM `siswa` JOIN kelas ON siswa.id_kelas = kelas.id_kelas";
                    $data = bacaSiswaJoinKelas($sql);
                    $x = 0;
                    $no = 1;
                    if($data != null){
                        foreach ($data as $baris){
                            $nis = $baris['nis'];
                            $nama_siswa = $baris['nama_siswa'];
                            $kelas = $baris['kelas'];
                            $jk = $baris['jk'];
                            $ttl = $baris['ttl'];
                            $alamat = $baris['alamat'];
                            $nama_ortu = $baris['nama_ortu'];

                            if($jk == "L" ){
                                $jk = "Laki-laki";
                            }else{
                                $jk = "Perempuan";
                            }
                ?>
                    <tr>
                        <td scope="row"><?php echo $no ?></td>                  
                        <td ><?php echo $nis ?></td>                  
                        <td><?php echo $nama_siswa ?></td>                
                        <td><?php echo $kelas ?></td>                
                        <td><?php echo $jk ?></td>                
                        <td><?php echo $ttl ?></td>                
                        <td><?php echo $alamat ?></td>                
                        <td><?php echo $nama_ortu ?></td>                                   
                    </tr>
                <?php
                        $no++;
                        $x++;
                        }
                    }else{  
                    echo "<td colspan='9'>Data Tidak Ada </td>";
                    }
                ?>
                </tbody>
            </table>
            </div>

            <div class="tab-pane fade" id="nav-nilai" role="tabpanel" aria-labelledby="nav-nilai-tab">
                <div class="form-group row" style="padding-top:2%;">
                    <div class="col-sm-2">Kelas</div>
                    <div class="col-sm-2">
                             <?php
                                $data = bacaSemuaKelas();
                                if($data != null){
                            ?>
                            <select class="custom-select" name="kelas" id="kelas1" onchange="getNilai()">
                                <option selected value="0">-Pilih Kelas-</option>
                                <?php
                                foreach ($data as $baris){
                                    $id = $baris['id_kelas'];
                                    $nama_kelas = $baris['kelas'];
                            ?>             
                                <option value="<?php echo $id ?> "><?php echo $nama_kelas ?></option>
                            <?php
                                }
                                }else{
                            ?>
                                <option selected value="0">-Pilih Kelas-</option>
                                <option value=" ">Tidak Ada Data</option>
                                <?php
                                }
                            ?>      
                            </select>
                    </div>       
                </div>
             ...<div id="formNilai"></div>
            </div>


            <div class="tab-pane fade" id="nav-jadwal" role="tabpanel" aria-labelledby="nav-jadwal-tab">
                <div class="form-group row" style="padding-top:2%;">
                    <div class="col-sm-2">Kelas</div>
                        <div class="col-sm-2">
                            <?php
                                $data = bacaSemuaKelas();
                                if($data != null){
                            ?>
                            <select class="custom-select" name="kelas" id="kelas">
                                <option selected value="0">-Pilih Kelas-</option>
                                <?php
                                foreach ($data as $baris){
                                    $id = $baris['id_kelas'];
                                    $nama_kelas = $baris['kelas'];
                            ?>             
                                <option value="<?php echo $id ?> "><?php echo $nama_kelas ?></option>
                            <?php
                                }
                                }else{
                            ?>
                                <option selected value="0">-Pilih Kelas-</option>
                                <option value=" ">Tidak Ada Data</option>
                                <?php
                                }
                            ?>      
                            </select>
                        </div>
                    </div>
                    <div id="formJadwal"></div>
                </div>
            </div>
        </div>
    </div>

<?php include 'footer.php' ?>
<script>
    $('#beranda').addClass('active');
    document.getElementById("kelas").onchange = function() {myFunction()};

    function search() {
        var input, filter, table, tr, td, i, txtValue;
        input = document.getElementById("nis");
        filter = input.value.toUpperCase();
        table = document.getElementById("tabelSiswa");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[1];
            if (td) {
                txtValue = td.textContent || td.innerText;
                if (txtValue.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }
    function searchGuru() {
        var input, filter, table, tr, td, i, txtValue;
        input = document.getElementById("idguru");
        filter = input.value.toUpperCase();
        table = document.getElementById("tabelGuru");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[1];
            if (td) {
                txtValue = td.textContent || td.innerText;
                if (txtValue.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }
    
  function myFunction() {
  var x = document.getElementById("kelas");

  var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        document.getElementById("formJadwal").innerHTML = this.responseText;
      }
    }          
    xhttp.open("GET", "formJadwal.php?id="+x.value, true);
    xhttp.send()
  }

  function getNilai() {
  var x = document.getElementById("kelas1");

  var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        document.getElementById("formNilai").innerHTML = this.responseText;
      }
    }          
    xhttp.open("GET", "formNilai.php?id="+x.value, true);
    xhttp.send()
  }

</script>