<?php include ('header.php');?>
<?php require_once ('controller/crudEkskul.php');?>
<?php
 if(isset($_SESSION['id_user'])){
     if($_SESSION['id_user']!=1){
        header("Location: 404.php");
     }
 } 

  if(isset($_GET['ubah'])){
    $id = $_GET['id_ekskul'];
    $data = cariEkskul($id);
    $id_ekskul = $data[0]['id_ekskul'];
    $nama_ekskul = $data[0]['nama_ekskul'];
    $button = "Ubah";
  }else{
    $button = "Tambah";
    $sql = "SELECT * from ekskul order by id_ekskul DESC LIMIT 1";
    $data = bacaEkskul($sql);
    $id_ekskul = 1;
    if ($data != null){
      $id_ekskul = $data[0]['id_ekskul']+1;
      $nama_ekskul = "";
    }else{
      $id_ekskul = 1;
      $nama_ekskul = "";
    }
  }
  //id
  


  if (isset($_GET['insert']) || isset($_GET['delete'])){
    $kata = "";
    $x = 0;
    $y = 0;
    if(isset($_GET['insert'])){
      $kata = "Ditambah";
      $x = 1;
      $y = 1;
    }elseif(isset($_GET['delete'])){
      $kata = "Dihapus";
      $x = 1;
      $y = 1;
    }else{
      $kata = "";
    } 
  
      if($x==0 || $y==0){
      ?>
      <div class="alert alert-danger" role="alert">
        <strong>Gagal!</strong> Data Gagal <?php echo $kata; ?> Silakan Cek Kembali
      </div>
  <?php
      }elseif($x==1 || $y==1){
  ?>
      
    <div class="alert alert-success" role="alert">
        <strong>Sukses!</strong> Data Berhasil <?php echo $kata; ?>
      </div>
  
  <?php
      }else{
      header("Location: 404.php");
    }
    }
  ?>
  <div class="container" style="margin-top:2%">
  <h3 style="text-align: center">Input Ekskul</h3>
  <form action="controller/insertEkskul.php" method="post">
    <div class="form-group row">
      <label for="id_ekskul" class="col-sm-2 col-form-label">ID Ekskul</label>
      <div class="col-sm-3">
        <input type="text" class="form-control" name="id_ekskul" value="<?php echo $id_ekskul ?> " id="#" readonly>
      </div>
    </div>
    <div class="form-group row">
      <label for="nama_ekskul" class="col-sm-2 col-form-label">Nama Ekskul</label>
      <div class="col-sm-3">
        <input type="text" class="form-control" name="nama_ekskul" value="<?php echo $nama_ekskul ?> ">
      </div>
    </div>
    <div class="form-group row">
      <div class="col-sm-10">
          <button type="submit" name="<?php echo $button ?>" class="btn btn-primary" ><?php echo $button ?></button>
      </div>
    </div>
   
    <br><hr>
    <table class="table table-striped table-bordered table-md" cellspacing="0" width="100%" id="tabelSiswa" >
			<thead class="thead-light">
				<tr>
					<th scope="col" >NO</th>
					<th scope="col" >ID Ekskul</th>
					<th scope="col" >Nama Ekskul</th>                       
					<th scope="col" >Aksi</th>                       
				</tr>
			</thead>
			<tbody>
      <?php
        $data = bacaSemuaekskul();
        $no = 1;
        if ($data != null){
          foreach($data as $baris){
            $id_ekskul = $baris['id_ekskul'];
            $nama_ekskul = $baris['nama_ekskul'];
      ?>
        <tr>
				    <td scope="row"><?php echo $no; ?>.</td>                  
				    <td scope="row"><?php echo $id_ekskul ?></td>                  
				    <td><?php echo $nama_ekskul ?></td>                
				    <td><a onclick="location.href='viewInputEkskul.php?ubah=1&id_ekskul=<?php echo $id_ekskul?>'" class="btn btn-outline-primary">EDIT</a> || <a class="btn btn-outline-danger" onclick="if(confirm('Apakah anda yakin ingin menghapus data ini ??')){ location.href='controller/deleteEkskul.php?id=<?php echo $id_ekskul; ?>' }">HAPUS</a></td>                    
			  </tr>
      <?php
            $no++;
          }
      ?>
      <?php
        }else{
          echo "<td colspan='3'>Data Tidak Ada </td>";
        }
      ?>
			</tbody>
		</table>
    <br>

		<!--ul class="pagination">
			<li class="page-item active"><a class="page-link" href="#">1</a><span class="sr-only">(current)</span></li>
			<li class="page-item">
			<a class="page-link" href="#" readonly>2 </a>
			</li>
			<li class="page-item"><a class="page-link" href="#">3</a></li>
		</ul-->		
    </div>
    

    
  </div>
  </form>
  </div>
<?php include 'footer.php' ?>
<script></script>