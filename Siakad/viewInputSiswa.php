<?php include 'header.php' ?>
<?php require_once ('controller/crudKelas.php');?>
<?php require_once ('controller/crudSiswa.php');?>
<?php require_once ('controller/crudEkskul.php');?>
<?php
 if(isset($_SESSION['id_user'])){
     if($_SESSION['id_user']!=1){
        header("Location: 404.php");
     }
 } 
?>

  <div class="container" style="margin-top:2%">
  <h3 style="text-align: center">Input Siswa</h3>
  <form action="controller/insertSiswa.php" method="post">
    <div class="form-group row">
      <div class="col-sm-2">Kelas</div>
        <div class="col-sm-2">
          <select class="custom-select" name="id_kelas">
          <?php
            $data = bacaSemuaKelas();
            if($data != null){
          ?>
              <option selected>-Pilih Kelas-</option>
          <?php
              foreach ($data as $baris){
                $id = $baris['id_kelas'];
                $nama_kelas = $baris['kelas'];
          ?>             
              <option value="<?php echo $id ?> "><?php echo $nama_kelas ?></option>
          <?php
              }
            }else{
          ?>
              <option selected>-Pilih Kelas-</option>
              <option value=" ">Tidak Ada Data</option>
              <?php
            }
          ?>            
          </select>
      </div>
    </div>
    <div class="form-group row">
      <div class="col-sm-2">Ekskul</div>
        <div class="col-sm-2">
          <select class="custom-select" name="id_ekskul">
          <?php
            $data = bacaSemuaEkskul();
            if($data != null){
          ?>
              <option selected>-Pilih Ekskul-</option>
          <?php
              foreach ($data as $baris){
                $id = $baris['id_ekskul'];
                $nama_ekskul = $baris['nama_ekskul'];
          ?>             
              <option value="<?php echo $id ?> "><?php echo $nama_ekskul ?></option>
          <?php
              }
            }else{
          ?>
              <option selected>-Pilih Ekskul-</option>
              <option value=" ">Tidak Ada Data</option>
              <?php
            }
          ?>            
          </select>
      </div>
    </div>
    <div class="form-group row">
      <label for="nama" class="col-sm-2 col-form-label">Nama</label>
      <div class="col-sm-5">
        <input type="text" class="form-control" name="nama" placeholder="Masukkan Nama Lengkap">
      </div>
    </div>
    <fieldset class="form-group">
      <div class="row">
        <legend class="col-form-label col-sm-2 pt-0">Jenis Kelamin</legend>
        <div class="col-sm-10">
          <div class="form-check">
            <input class="form-check-input" type="radio" name="JK" name="L" value="L" checked>
            <label class="form-check-label" for="L">
            Laki-Laki
            </label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="radio" name="JK" name="P" value="P">
            <label class="form-check-label" for="P">
            Perempuan
            </label>
          </div>
        </div>
      </div>
    </fieldset>

    <div class="form-group row">
      <label for="tanggal_lahir" class="col-sm-2 col-form-label">Tanggal Lahir</label>
      <div class="col-sm-3">
          <input type="date" class="form-control" name="tanggal_lahir"></input>
      </div>
    </div>
    
    <div class="form-group row">
      <label for="alamat" class="col-sm-2 col-form-label">Alamat</label>
      <div class="col-sm-6">
          <textarea class="form-control" name="alamat" placeholder="Masukan Alamat Rumah Lengkap" ></textarea>
      </div>
    </div>

    <div class="form-group row">
      <label for="nama_ortu" class="col-sm-2 col-form-label">Nama Orang Tua/ Wali Murid</label>
      <div class="col-sm-5">
        <input type="text" class="form-control" name="nama_ortu" id="nama_ortu" placeholder="Masukkan Nama Orang Tua">
      </div>
    </div>

    <div class="form-group row">
      <label for="alamat_ortu" class="col-sm-2 col-form-label">Alamat Orang Tua/ Wali Murid</label>
      <div class="col-sm-6">
          <textarea class="form-control" name="alamat_ortu" placeholder="Masukan Alamat Rumah Lengkap" ></textarea>
      </div>
    </div>
    <div class="form-group row">
      <label for="pwd" class="col-sm-2 col-form-label">Password</label>
      <div class="col-sm-3">
        <input type="password" class="form-control" name="pwd" id="pwd" placeholder="Masukkan Password">
      </div>
    </div>

    <div class="form-group row">
      <div class="col-sm-10">
          <button type="submit" name="Tambah" class="btn btn-primary">Tambah</button>
          <button type="submit" name="Batal" class="btn btn-primary">Batal</button>
      </div>
    </div>
  </div>
  </form>
  </div>
<?php include 'footer.php' ?>
<script></script>