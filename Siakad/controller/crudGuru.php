<?php 
	require_once ('koneksiDb.php');
	
	function bacaGuru($sql){
		$data = array();
		$koneksi = koneksi();
		$hasil = mysqli_query($koneksi, $sql);
		
		if(mysqli_num_rows($hasil) == 0){
			mysqli_close($koneksi);
			return null;
		}
		
		$i=0;
		while ($baris = mysqli_fetch_array($hasil)){			
			$data[$i]['id_guru'] = $baris['id_guru'];
			$data[$i]['id_mapel'] = $baris['id_mapel'];
			$data[$i]['id_user'] = $baris['id_user'];
			$data[$i]['nama'] = $baris['nama'];
			$data[$i]['jk'] = $baris['jk'];
			$data[$i]['jabatan'] = $baris['jabatan'];
			$data[$i]['no_hp'] = $baris['no_hp'];
			$data[$i]['password'] = $baris['password'];
			$i++;
		}
		mysqli_close($koneksi);
		return $data;
	}
	function bacaSemuaGuru(){
		$sql = "SELECT * from Guru";
		$data = bacaGuru($sql);
		return $data;
	}
	function cariGuru($id_guru){
		$sql = "SELECT * from Guru where id_guru=$id_guru";
		$data = bacaGuru($sql);
		return $data;
	}

	function tambahGuru($id_guru, $id_mapel, $id_user, $nama, $jk, $jabatan, $no_hp,$password){
		$koneksi = koneksi();
		$sql = "INSERT into Guru values($id_guru, $id_mapel, $id_user, '$nama', '$jk', '$jabatan', '$no_hp','$password')";
		$hasil = 0;
		if(mysqli_query($koneksi, $sql))
			$hasil = 1;
		mysqli_close($koneksi);
		return $hasil;
	}

	function ubahGuru($id_guru, $id_mapel, $id_user, $nama, $jk, $jabatan, $no_hp,$password){
		$koneksi = koneksi();
		$sql = "Update Guru SET id_mapel = $id_mapel, id_user = $id_user, nama = '$nama', jk ='$jk', jabatan = '$jabatan', no_hp ='$no_hp', password = '$password' where id_guru=$id_guru";
		$hasil = 0;
		if(mysqli_query($koneksi, $sql))
			$hasil = 1;
		mysqli_close($koneksi);
		return $hasil;
	}
	
	// menghapus 1 record berdasar field kunci kode
	function hapusGuru($id){
		$koneksi = koneksi();
		$sql = "DELETE from Guru where id_guru=$id";
		if (!mysqli_query($koneksi, $sql)){
			die('Error: ' . mysqli_error($koneksi));
		}
		$hasil = mysqli_affected_rows($koneksi);
		mysqli_close($koneksi);
		return $hasil;
	}

	//join mapel
	function bacaGuruJoinMapel($sql){
		$data = array();
		$koneksi = koneksi();
		$hasil = mysqli_query($koneksi, $sql);
		
		if(mysqli_num_rows($hasil) == 0){
			mysqli_close($koneksi);
			return null;
		}
		
		$i=0;
		while ($baris = mysqli_fetch_array($hasil)){			
			$data[$i]['id_guru'] = $baris['id_guru'];
			$data[$i]['id_mapel'] = $baris['id_mapel'];
			$data[$i]['id_user'] = $baris['id_user'];
			$data[$i]['nama'] = $baris['nama'];
			$data[$i]['jk'] = $baris['jk'];
			$data[$i]['jabatan'] = $baris['jabatan'];
			$data[$i]['no_hp'] = $baris['no_hp'];
			$data[$i]['password'] = $baris['password'];
			$data[$i]['id_mapel'] = $baris['id_mapel'];
			$data[$i]['nama_mapel'] = $baris['nama_mapel'];
			$i++;
		}
		mysqli_close($koneksi);
		return $data;
	}
?>