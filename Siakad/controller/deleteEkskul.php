<?php
require_once ('crudEkskul.php');

if(isset($_GET['id'])){
    $id_ekskul = $_GET['id'];

    $delete = hapusEkskul($id_ekskul);
    if($delete>0){
        header("Location: ../viewInputEkskul.php?delete=1");
    }else{
        header("Location: ../viewInputEkskul.php?delete=0");
    }
}else{
    header("Location: ../404.php");
}
?>