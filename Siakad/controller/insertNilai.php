<?php
require_once ('crudNilai.php');
require_once ('crudDetailNilai.php');

if(isset($_POST['Simpan'])){
    $id_guru = $_POST['id_guru'];
    $nis = $_POST['nis'];
    $id_mapel = $_POST['id_mapel'];
    $semester = $_POST['semester'];
    $tahun = date("Y");
    $tugas1 = $_POST['tugas1'];
    $tugas2 = $_POST['tugas2'];
    $uh1 = $_POST['uh1'];
    $uh2 = $_POST['uh2'];
    $uts = $_POST['uts'];
    $uas = $_POST['uas'];
    $nilai_ekskul = $_POST['nilai_ekskul'];
    $id_Nilai = $_POST['id_nilai'];;
    $id_detailNilai = $_POST['id_detailNilai'];

   print("<pre>".print_r($_POST,true)."</pre>"); 

    $i = 0;
    while($i < count($_POST['nis'])){
        $np[$i] = ($tugas1[$i]+$tugas2[$i]+$uh1[$i]+$uh2[$i])/4;
        $np[$i] = number_format($np[$i],2);
        $nilai[$i] = (2*$np[$i]+1*$uts[$i]+1*$uas[$i])/4;
        $nilai[$i] = ($nilai[$i]/100)*4;

        if(3.66 < $nilai[$i] && $nilai[$i] <= 4 ){
            $keterangan[$i] = "A";
        }elseif(3.33<$nilai[$i] && $nilai[$i] <= 3.66){
            $keterangan[$i] = "A-";
        }elseif(3<$nilai[$i] && $nilai[$i] <= 3.33){
            $keterangan[$i] = "B+";
        }elseif(2.66<$nilai[$i] && $nilai[$i] <= 3){
            $keterangan[$i] = "B";
        }elseif(2.33<$nilai[$i] && $nilai[$i] <= 2.66){
            $keterangan[$i] = "B-";
        }elseif(2<$nilai[$i] && $nilai[$i] <= 2.33){
            $keterangan[$i] = "C+";
        }elseif(1.66<$nilai[$i] && $nilai[$i] <= 2){
            $keterangan[$i] = "C";
        }elseif(1.33<$nilai[$i] && $nilai[$i] <= 1.66){
            $keterangan[$i] = "C-";
        }elseif(1<$nilai[$i] && $nilai[$i] <= 1.33){
            $keterangan[$i] = "D+";
        }elseif(0<$nilai[$i] && $nilai[$i] <= 1){
            $keterangan[$i] = "D";
        }else{
            $keterangan[$i] = "-";
            
        }       
        $sql = "UPDATE `detail_nilai` SET Tugas1 = $tugas1[$i], Tugas2=$tugas2[$i], UH1=$uh1[$i], UH2=$uh2[$i], UTS=$uts[$i], UAS=$uas[$i], nilai_ekskul =$nilai_ekskul[$i] WHERE id_detailNilai = $id_detailNilai[$i]";
        echo "$sql <br>";

        $tambahNilai = ubahNilai($id_Nilai[$i], $id_guru, $nis[$i], $id_mapel, $semester, $tahun, $nilai[$i],$keterangan[$i]);
        $tambahDetail = ubahDetailNilai($id_detailNilai[$i], $tugas1[$i], $tugas2[$i], $uh1[$i], $uh2[$i], $uts[$i], $uas[$i], $nilai_ekskul[$i]);

        $i++;
    }
    if($tambahNilai>0 && $tambahDetail){
        if(isset($_GET['guru'])){
            header("Location: ../guru/viewInputNilai.php?insert=1");
        }else{
            header("Location: ../viewInputNilai.php?insert=1");
        }
    }else{
        if(isset($_GET['guru'])){
            header("Location: ../guru/viewInputNilai.php?insert=0");
        }else{
            header("Location: ../viewInputNilai.php?insert=0");
        }
    }
}elseif(isset($_POST['Batal'])){
    header("Location: ../viewInputNilai.php");
}else{
    header("Location: ../404.php");
}
?>