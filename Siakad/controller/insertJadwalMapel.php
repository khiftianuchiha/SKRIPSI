<?php
require_once ('crudJadwalPelajaran.php');

if(isset($_POST['Tambah'])){
    $id_kelas = $_POST['kelas'];
    $id_mapel = $_POST['id_mapel'];
    $id_guru = $_POST['guru'];
    $hari = $_POST['hari'];
    $jam = $_POST['jam'];

    $sql = "SELECT * from `jadwal pelajaran` order by id_jadwal DESC LIMIT 1";
    $data = bacaJadwalPelajaran($sql);
    $id_jadwal = 1;
    if ($data != null){
        $id_jadwal = $data[0]['id_jadwal']+1;
    }else{
        $id_jadwal = 1;
    }

    if($jam == 1){
        $jam_mulai = "07:00:00";
        $jam_selesai = "09:00:00";
    }elseif($jam == 2){
        $jam_mulai = "09:00:00";
        $jam_selesai = "11:00:00";
    }elseif($jam == 3){
        $jam_mulai = "12:00:00";
        $jam_selesai = "14:00:00";
    }elseif($jam == 4){
        $jam_mulai = "14:00:00";
        $jam_selesai = "16:00:00";
    }elseif($jam == 5){
        $jam_mulai = "16:00:00";
        $jam_selesai = "18:00:00";
    }
    
    $sql = "SELECT * from `jadwal pelajaran` where id_guru= $id_guru AND id_kelas=$id_kelas AND hari = '$hari' AND jam_mulai='$jam_mulai'";
    $data = bacaJadwalPelajaran($sql);
    
    if($data != null){        
        header("Location: ../viewInputJadwal.php?insert=0");
    }else{
        $sql = "SELECT * from `jadwal pelajaran` where id_guru=$id_guru AND hari = '$hari' AND jam_mulai='$jam_mulai'";
        $data = bacaJadwalPelajaran($sql);
        if($data != null){   
           header("Location: ../viewInputJadwal.php?insert=0");
        }else{
            $sql = "SELECT * from `jadwal pelajaran` where hari = '$hari' AND jam_mulai='$jam_mulai'";
            $data = bacaJadwalPelajaran($sql);
            if($data != null){
                header("Location: ../viewInputJadwal.php?insert=0");
            }else{
                $insert = tambahJadwalPelajaran($id_jadwal, $id_kelas, $id_guru, $hari, $jam_mulai,$jam_selesai);
                if($insert>0){
                    header("Location: ../viewInputJadwal.php?insert=1");
                }else{
                    header("Location: ../viewInputJadwal.php?insert=0");
                }
            }
        }          
    }

    
}else{
    header("Location: ../404.php");
}
?>