<?php
require_once ('crudMapel.php');

if(isset($_GET['id'])){
    $id_mapel = $_GET['id'];

    $delete = hapusMapel($id_mapel);
    if($delete>0){
        header("Location: ../viewInputMapel.php?delete=1");
    }else{
        header("Location: ../viewInputMapel.php?delete=0");
    }
}else{
    header("Location: ../404.php");
}
?>