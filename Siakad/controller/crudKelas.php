<?php 
	require_once ("koneksiDb.php");
	
	function bacaKelas($sql){
		$data = array();
		$koneksi = koneksi();
		$hasil = mysqli_query($koneksi, $sql);
		
		if(mysqli_num_rows($hasil) == 0){
			mysqli_close($koneksi);
			return null;
		}
		
		$i=0;
		while ($baris = mysqli_fetch_array($hasil)){			
			$data[$i]['id_kelas'] = $baris['id_kelas'];
			$data[$i]['kelas'] = $baris['kelas'];
			$i++;
		}
		mysqli_close($koneksi);
		return $data;
	}
	function bacaSemuaKelas(){
		$sql = "SELECT * from Kelas order by id_kelas";
		$data = bacaKelas($sql);
		return $data;
	}
	function cariKelas($id_Kelas){
		$sql = "SELECT * from Kelas where id_kelas=$id_Kelas ";
		$data = bacaKelas($sql);
		return $data;
	}

	function tambahKelas($id_Kelas, $nama_Kelas){
		$koneksi = koneksi();
		$sql = "INSERT into Kelas values($id_Kelas,'$nama_Kelas')";
		$hasil = 0;
		if(mysqli_query($koneksi, $sql))
			$hasil = 1;
		mysqli_close($koneksi);
		return $hasil;
	}
	function ubahKelas($id_Kelas, $nama_Kelas){
		$koneksi = koneksi();
		$sql = "Update Kelas set kelas ='$nama_Kelas' where id_kelas = $id_Kelas";
		$hasil = 0;

		if(mysqli_query($koneksi, $sql))
			$hasil = 1;
		mysqli_close($koneksi);
		return $hasil;
	}
	
	// menghapus 1 record berdasar field kunci kode
	function hapusKelas($id){
		$koneksi = koneksi();
		$sql = "DELETE from Kelas where id_kelas=$id";
		if (!mysqli_query($koneksi, $sql)){
			die('Error: ' . mysqli_error($koneksi));
		}
		$hasil = mysqli_affected_rows($koneksi);
		mysqli_close($koneksi);
		return $hasil;
	}
	function bacaKelasJoinJadwal($sql){
		$data = array();
		$koneksi = koneksi();
		$hasil = mysqli_query($koneksi, $sql);
		
		if(mysqli_num_rows($hasil) == 0){
			mysqli_close($koneksi);
			return null;
		}
		
		$i=0;
		while ($baris = mysqli_fetch_array($hasil)){			
			$data[$i]['id_kelas'] = $baris['id_kelas'];
			$data[$i]['kelas'] = $baris['kelas'];
			$data[$i]['id_jadwal'] = $baris['id_jadwal'];
			$data[$i]['id_guru'] = $baris['id_guru'];
			$data[$i]['id_kelas'] = $baris['id_kelas'];
			$data[$i]['hari'] = $baris['hari'];
			$data[$i]['jam_mulai'] = $baris['jam_mulai'];
			$data[$i]['jam_selesai'] = $baris['jam_selesai'];
			$i++;
		}
		mysqli_close($koneksi);
		return $data;
	}
?>