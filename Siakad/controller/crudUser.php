<?php 
	require_once ('koneksiDb.php');
	
	function bacaUser($sql){
		$data = array();
		$koneksi = koneksi();
		$hasil = mysqli_query($koneksi, $sql);
		
		if(mysqli_num_rows($hasil) == 0){
			mysqli_close($koneksi);
			return null;
		}
		
		$i=0;
		while ($baris = mysqli_fetch_array($hasil)){			
			$data[$i]['id_user'] = $baris['id_user'];
			$data[$i]['nama_user'] = $baris['nama_user'];
			$i++;
		}
		mysqli_close($koneksi);
		return $data;
	}
	function bacaSemuaUser(){
		$sql = "SELECT * from User";
		$data = bacaUser($sql);
		return $data;
	}
	function cariUser($id_User){
		$sql = "SELECT * from User where id_user=$id_User";
		$data = bacaUser($sql);
		return $data;
	}

	function tambahUser($id_User, $nama_user){
		$koneksi = koneksi();
		$sql = "INSERT into User values($id_User,'$nama_user')";
		$hasil = 0;
		if(mysqli_query($koneksi, $sql))
			$hasil = 1;
		mysqli_close($koneksi);
		return $hasil;
	}
	
	// menghapus 1 record berdasar field kunci kode
	function hapusUser($id){
		$koneksi = koneksi();
		$sql = "DELETE from User where id_user='$id'";
		if (!mysqli_query($koneksi, $sql)){
			die('Error: ' . mysqli_error($koneksi));
		}
		$hasil = mysqli_affected_rows($koneksi);
		mysqli_close($koneksi);
		return $hasil;
	}

	function bacaUserJoinGuru($sql){
		$data = array();
		$koneksi = koneksi();
		$hasil = mysqli_query($koneksi, $sql);
		
		if(mysqli_num_rows($hasil) == 0){
			mysqli_close($koneksi);
			return null;
		}
		
		$i=0;
		while ($baris = mysqli_fetch_array($hasil)){			
			$data[$i]['id_user'] = $baris['id_user'];
			$data[$i]['nama_user'] = $baris['nama_user'];
			$data[$i]['id_guru'] = $baris['id_guru'];
			$data[$i]['id_mapel'] = $baris['id_mapel'];
			$data[$i]['id_user'] = $baris['id_user'];
			$data[$i]['nama'] = $baris['nama'];
			$data[$i]['jk'] = $baris['jk'];
			$data[$i]['jabatan'] = $baris['jabatan'];
			$data[$i]['no_hp'] = $baris['no_hp'];
			$data[$i]['password'] = $baris['password'];
			$i++;
		}
		mysqli_close($koneksi);
		return $data;
	}
	function bacaUserJoinSiswa($sql){
		$data = array();
		$koneksi = koneksi();
		$hasil = mysqli_query($koneksi, $sql);
		
		if(mysqli_num_rows($hasil) == 0){
			mysqli_close($koneksi);
			return null;
		}
		
		$i=0;
		while ($baris = mysqli_fetch_array($hasil)){			
			$data[$i]['id_user'] = $baris['id_user'];
			$data[$i]['nama_user'] = $baris['nama_user'];
			$data[$i]['nis'] = $baris['nis'];
			$data[$i]['id_user'] = $baris['id_user'];
			$data[$i]['id_kelas'] = $baris['id_kelas'];
			$data[$i]['id_ekskul'] = $baris['id_ekskul'];
			$data[$i]['password'] = $baris['password'];
			$data[$i]['nama_siswa'] = $baris['nama_siswa'];
			$data[$i]['jk'] = $baris['jk'];
			$data[$i]['alamat'] = $baris['alamat'];
			$data[$i]['ttl'] = $baris['ttl'];
			$data[$i]['nama_ortu'] = $baris['nama_ortu'];
			$data[$i]['alamat_ortu'] = $baris['alamat_ortu'];
			$i++;
		}
		mysqli_close($koneksi);
		return $data;
	}
?>