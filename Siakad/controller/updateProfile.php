<?php
require_once ('crudGuru.php');
require_once ('crudSiswa.php');

if(isset($_POST['simpan'])){
    
    $id = $_POST['id'];
    $nama = $_POST['nama'];
    $jk = $_POST['jk'];
    $password = $_POST['pwd'];
    $id_user = $_POST['id_user'];

    if($_POST['id_user']==2){
        $cari = cariSiswa($id);
        $update = ubahSiswa($id, $id_user, $cari[0]['id_kelas'], $cari[0]['id_ekskul'],$password,$nama,$jk,$cari[0]['alamat'],$cari[0]['ttl'],$cari[0]['nama_ortu'],$cari[0]['alamat_ortu']);

    }else{
        $cari = cariGuru($id);
        $update = ubahGuru($id, $cari[0]['id_mapel'], $id_user, $nama, $jk, $cari[0]['jabatan'], $cari[0]['no_hp'],$password);
    }

   
    if($update>0){
       header("Location: ../profilku.php?insert=1");
    }else{
       header("Location: ../profilku.php?insert=0");
    }
}elseif(isset($_POST['Batal'])){
    header("Location: ../profilku.php");
}else{
    header("Location: ../404.php");
}
?>