<?php
require_once ('crudKelas.php');

if(isset($_GET['id'])){
    $id_kelas = $_GET['id'];

    $delete = hapusKelas($id_kelas);
    if($delete>0){
        header("Location: ../viewInputKelas.php?delete=1");
    }else{
        header("Location: ../viewInputKelas.php?delete=0");
    }
}else{
    header("Location: ../404.php");
}
?>