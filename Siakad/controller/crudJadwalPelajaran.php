<?php 
	require_once ('koneksiDb.php');
	
	function bacaJadwalPelajaran($sql){
		$data = array();
		$koneksi = koneksi();
		$hasil = mysqli_query($koneksi, $sql);
		
		if(mysqli_num_rows($hasil) == 0){
			mysqli_close($koneksi);
			return null;
		}
		
		$i=0;
		while ($baris = mysqli_fetch_array($hasil)){			
			$data[$i]['id_jadwal'] = $baris['id_jadwal'];
			$data[$i]['id_guru'] = $baris['id_guru'];
			$data[$i]['id_kelas'] = $baris['id_kelas'];
			$data[$i]['hari'] = $baris['hari'];
			$data[$i]['jam_mulai'] = $baris['jam_mulai'];
			$data[$i]['jam_selesai'] = $baris['jam_selesai'];
			$i++;
		}
		mysqli_close($koneksi);
		return $data;
	}
	function bacaSemuaJadwalPelajaran(){
		$sql = "SELECT * from `jadwal pelajaran`";
		$data = bacaJadwalPelajaran($sql);
		return $data;
	}
	function cariJadwalPelajaran($id_JadwalPelajaran){
		$sql = "SELECT * from `jadwal pelajaran` where id_jadwal=$id_JadwalPelajaran ";
		$data = bacaJadwalPelajaran($sql);
		return $data;
	}
	function cariJadwalPelajaranGuru($id_guru){
		$sql = "SELECT * from `jadwal pelajaran` where id_guru=$id_guru ";
		$data = bacaJadwalPelajaran($sql);
		return $data;
	}

	function tambahJadwalPelajaran($id_JadwalPelajaran, $id_kelas, $id_guru, $hari, $jam_mulai,$jam_selesai){
		$koneksi = koneksi();
		$sql = "INSERT into `jadwal pelajaran` values($id_JadwalPelajaran, $id_kelas, $id_guru, '$hari', '$jam_mulai','$jam_selesai')";
		$hasil = 0;
		if(mysqli_query($koneksi, $sql))
			$hasil = 1;
		mysqli_close($koneksi);
		return $hasil;
	}
	
	// menghapus 1 record berdasar field kunci kode
	function hapusJadwalPelajaran($id){
		$koneksi = koneksi();
		$sql = "DELETE from `jadwal pelajaran` where id_jadwal=$id";
		if (!mysqli_query($koneksi, $sql)){
			die('Error: ' . mysqli_error($koneksi));
		}
		$hasil = mysqli_affected_rows($koneksi);
		mysqli_close($koneksi);
		return $hasil;
	}

	// menghapus 1 record berdasar field kunci kode
	function hapusJadwalPelajaranKelas($id){
		$koneksi = koneksi();
		$sql = "DELETE from `jadwal pelajaran` where id_kelas=$id";
		if (!mysqli_query($koneksi, $sql)){
			die('Error: ' . mysqli_error($koneksi));
		}
		$hasil = mysqli_affected_rows($koneksi);
		mysqli_close($koneksi);
		return $hasil;
	}

	function bacaJadwalPelajaranJoinGuruMapel($sql){
		$data = array();
		$koneksi = koneksi();
		$hasil = mysqli_query($koneksi, $sql);
		
		if(mysqli_num_rows($hasil) == 0){
			mysqli_close($koneksi);
			return null;
		}
		
		$i=0;
		while ($baris = mysqli_fetch_array($hasil)){			
			$data[$i]['id_jadwal'] = $baris['id_jadwal'];
			$data[$i]['id_guru'] = $baris['id_guru'];
			$data[$i]['id_kelas'] = $baris['id_kelas'];
			$data[$i]['hari'] = $baris['hari'];
			$data[$i]['jam_mulai'] = $baris['jam_mulai'];
			$data[$i]['jam_selesai'] = $baris['jam_selesai'];
			$data[$i]['id_guru'] = $baris['id_guru'];
			$data[$i]['id_mapel'] = $baris['id_mapel'];
			$data[$i]['id_user'] = $baris['id_user'];
			$data[$i]['nama'] = $baris['nama'];
			$data[$i]['jk'] = $baris['jk'];
			$data[$i]['jabatan'] = $baris['jabatan'];
			$data[$i]['no_hp'] = $baris['no_hp'];
			$data[$i]['password'] = $baris['password'];
			$data[$i]['id_mapel'] = $baris['id_mapel'];
			$data[$i]['nama_mapel'] = $baris['nama_mapel'];
			$i++;
		}
		mysqli_close($koneksi);
		return $data;
	}
	function bacaJadwalPelajaranJoinGuruKelas($sql){
		$data = array();
		$koneksi = koneksi();
		$hasil = mysqli_query($koneksi, $sql);
		
		if(mysqli_num_rows($hasil) == 0){
			mysqli_close($koneksi);
			return null;
		}
		
		$i=0;
		while ($baris = mysqli_fetch_array($hasil)){			
			$data[$i]['id_jadwal'] = $baris['id_jadwal'];
			$data[$i]['id_guru'] = $baris['id_guru'];
			$data[$i]['id_kelas'] = $baris['id_kelas'];
			$data[$i]['hari'] = $baris['hari'];
			$data[$i]['jam_mulai'] = $baris['jam_mulai'];
			$data[$i]['jam_selesai'] = $baris['jam_selesai'];
			$data[$i]['id_guru'] = $baris['id_guru'];
			$data[$i]['id_mapel'] = $baris['id_mapel'];
			$data[$i]['id_user'] = $baris['id_user'];
			$data[$i]['nama'] = $baris['nama'];
			$data[$i]['jk'] = $baris['jk'];
			$data[$i]['jabatan'] = $baris['jabatan'];
			$data[$i]['no_hp'] = $baris['no_hp'];
			$data[$i]['password'] = $baris['password'];
			$data[$i]['id_kelas'] = $baris['id_kelas'];
			$data[$i]['kelas'] = $baris['kelas'];
			
			$i++;
		}
		mysqli_close($koneksi);
		return $data;
	}
?>