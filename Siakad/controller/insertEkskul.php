<?php
require_once ('crudEkskul.php');

if(isset($_POST['Tambah'])){
    $idekskul = $_POST['id_ekskul'];
    $namaekskul = strtoupper($_POST['nama_ekskul']);

    $insert = tambahEkskul($idekskul, $namaekskul);
    if($insert>0){
        header("Location: ../viewInputEkskul.php?insert=1");
    }else{
        header("Location: ../viewInputEkskul.php?insert=0");
    }
}elseif(isset($_POST['Ubah'])){
    $idekskul = $_POST['id_ekskul'];
    $namaekskul = strtoupper($_POST['nama_ekskul']);
    $update = ubahEkskul($idekskul, $namaekskul);
    if($update>0){
        header("Location: ../viewInputEkskul.php?insert=1");
    }else{
        header("Location: ../viewInputEkskul.php?insert=0");
    }
}else{
    header("Location: ../404.php");
}
?>