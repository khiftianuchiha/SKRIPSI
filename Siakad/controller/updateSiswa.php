<?php
require_once ('crudSiswa.php');

if(isset($_POST['Tambah'])){
    $nis = $_POST['nis'];
    $nama_siswa = $_POST['nama'];
    $jk = $_POST['JK'];
    $alamat = $_POST['alamat'];
    $nama_ortu = $_POST['nama_ortu'];
    $alamat_ortu = $_POST['alamat_ortu'];
    $id_ekskul = $_POST['id_ekskul'];
    $password = $_POST['pwd'];
    $id_user = 2; //Siswa
    $id_kelas = $_POST['id_kelas'];
    $ttl = $_POST['tanggal_lahir'];

   $update = ubahSiswa($nis, $id_user, $id_kelas, $id_ekskul,$password,$nama_siswa,$jk,$alamat,$ttl,$nama_ortu,$alamat_ortu);
    if($update>0){
       header("Location: ../viewSiswa.php?insert=1");
    }else{
      header("Location: ../viewSiswa.php?insert=0");
    }
}elseif(isset($_POST['Batal'])){
    header("Location: ../viewGuru.php");
}else{
    header("Location: ../404.php");
}
?>