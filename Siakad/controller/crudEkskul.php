<?php 
	require_once ('koneksiDb.php');
	
	function bacaEkskul($sql){
		$data = array();
		$koneksi = koneksi();
		$hasil = mysqli_query($koneksi, $sql);
		
		if(mysqli_num_rows($hasil) == 0){
			mysqli_close($koneksi);
			return null;
		}
		
		$i=0;
		while ($baris = mysqli_fetch_array($hasil)){			
			$data[$i]['id_ekskul'] = $baris['id_ekskul'];
			$data[$i]['nama_ekskul'] = $baris['nama_ekskul'];
			$i++;
		}
		mysqli_close($koneksi);
		return $data;
	}
	function bacaSemuaEkskul(){
		$sql = "SELECT * from Ekskul order by id_ekskul";
		$data = bacaEkskul($sql);
		return $data;
	}
	function cariEkskul($id_Ekskul){
		$sql = "SELECT * from Ekskul where id_ekskul=$id_Ekskul";
		$data = bacaEkskul($sql);
		return $data;
	}

	function tambahEkskul($id_Ekskul, $nama_Ekskul){
		$koneksi = koneksi();
		$sql = "INSERT into Ekskul values($id_Ekskul,'$nama_Ekskul')";
		$hasil = 0;
		if(mysqli_query($koneksi, $sql))
			$hasil = 1;
		mysqli_close($koneksi);
		return $hasil;
	}

	function ubahEkskul($id_Ekskul, $nama_Ekskul){
		$koneksi = koneksi();
		$sql = "Update Ekskul set nama_ekskul ='$nama_Ekskul' where id_ekskul = $id_Ekskul";
		$hasil = 0;

		if(mysqli_query($koneksi, $sql))
			$hasil = 1;
		mysqli_close($koneksi);
		return $hasil;
	}
	
	// menghapus 1 record berdasar field kunci kode
	function hapusEkskul($id){
		$koneksi = koneksi();
		$sql = "DELETE from Ekskul where id_ekskul='$id'";
		if (!mysqli_query($koneksi, $sql)){
			die('Error: ' . mysqli_error($koneksi));
		}
		$hasil = mysqli_affected_rows($koneksi);
		mysqli_close($koneksi);
		return $hasil;
	}
?>