<?php 
	require_once ('koneksiDb.php');
	
	function bacaDetailNilai($sql){
		$data = array();
		$koneksi = koneksi();
		$hasil = mysqli_query($koneksi, $sql);
		
		if(mysqli_num_rows($hasil) == 0){
			mysqli_close($koneksi);
			return null;
		}
		
		$i=0;
		while ($baris = mysqli_fetch_array($hasil)){			
			$data[$i]['id_detailNilai'] = $baris['id_detailNilai'];
			$data[$i]['id_nilai'] = $baris['id_nilai'];
			$data[$i]['Tugas1'] = $baris['Tugas1'];
			$data[$i]['Tugas2'] = $baris['Tugas2'];
			$data[$i]['UH1'] = $baris['UH1'];
			$data[$i]['UH2'] = $baris['UH2'];
			$data[$i]['UTS'] = $baris['UTS'];
			$data[$i]['UAS'] = $baris['UAS'];
			$data[$i]['nilai_ekskul'] = $baris['nilai_ekskul'];
			$i++;
		}
		mysqli_close($koneksi);
		return $data;
	}
	function bacaSemuaDetailNilai(){
		$sql = "SELECT * from `detail_nilai`";
		$data = bacaDetailNilai($sql);
		return $data;
	}
	function cariDetailNilai($id_detailNilai){
		$sql = "SELECT * from `detail_nilai` where id_detailNilai=$id_detailNilai";
		$data = bacaDetailNilai($sql);
		return $data;
	}

	function tambahDetailNilai($id_detailNilai, $id_nilai, $tugas1, $tugas2, $uh1, $uh2, $uts, $uas, $nilai_ekskul){
		$koneksi = koneksi();
		$sql = "INSERT into `detail_nilai` values($id_detailNilai, $id_nilai,$tugas1, $tugas2, $uh1, $uh2, $uts, $uas, $nilai_ekskul)";
		$hasil = 0;
		if(mysqli_query($koneksi, $sql))
			$hasil = 1;
		mysqli_close($koneksi);
		return $hasil;
	}
	function ubahDetailNilai($id_detailNilai, $tugas1, $tugas2, $uh1, $uh2, $uts, $uas, $nilai_ekskul){
		$koneksi = koneksi();
		$sql = "UPDATE `detail_nilai` SET Tugas1 = $tugas1, Tugas2=$tugas2, UH1=$uh1, UH2=$uh2, UTS=$uts, UAS=$uas, nilai_ekskul =$nilai_ekskul WHERE id_detailNilai = $id_detailNilai";
		$hasil = 0;
		if(mysqli_query($koneksi, $sql))
			$hasil = 1;
		mysqli_close($koneksi);
		return $hasil;
	}
	
	// menghapus 1 record berdasar field kunci kode
	function hapusDetailNilai($id){
		$koneksi = koneksi();
		$sql = "DELETE from `deatail_nilai` where id_detailNilai=$id";
		if (!mysqli_query($koneksi, $sql)){
			die('Error: ' .mysqli_error($koneksi));
		}
		$hasil = mysqli_affected_rows($koneksi);
		mysqli_close($koneksi);
		return $hasil;
	}
	function bacaDetailNilaiJoinNilai($sql){
		$data = array();
		$koneksi = koneksi();
		$hasil = mysqli_query($koneksi, $sql);
		
		if(mysqli_num_rows($hasil) == 0){
			mysqli_close($koneksi);
			return null;
		}
		
		$i=0;
		while ($baris = mysqli_fetch_array($hasil)){			
			$data[$i]['id_detailNilai'] = $baris['id_detailNilai'];
			$data[$i]['id_nilai'] = $baris['id_nilai'];
			$data[$i]['Tugas1'] = $baris['Tugas1'];
			$data[$i]['Tugas2'] = $baris['Tugas2'];
			$data[$i]['UH1'] = $baris['UH1'];
			$data[$i]['UH2'] = $baris['UH2'];
			$data[$i]['UTS'] = $baris['UTS'];
			$data[$i]['UAS'] = $baris['UAS'];
			$data[$i]['nilai_ekskul'] = $baris['nilai_ekskul'];
			$data[$i]['id_nilai'] = $baris['id_nilai'];
			$data[$i]['id_guru'] = $baris['id_guru'];
			$data[$i]['nis'] = $baris['nis'];
			$data[$i]['id_mapel'] = $baris['id_mapel'];
			$data[$i]['semester'] = $baris['semester'];
			$data[$i]['tahun'] = $baris['tahun'];
			$data[$i]['nilai'] = $baris['nilai'];
			$data[$i]['keterangan'] = $baris['keterangan'];
			$i++;
		}
		mysqli_close($koneksi);
		return $data;
	}
?>