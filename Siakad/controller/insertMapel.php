<?php
require_once ('crudMapel.php');

if(isset($_POST['Tambah'])){
    $idMapel = $_POST['id_mapel'];
    $namaMapel = strtoupper($_POST['nama_mapel']);

    $insert = tambahMapel($idMapel, $namaMapel);
    if($insert>0){
        header("Location: ../viewInputMapel.php?insert=1");
    }else{
        header("Location: ../viewInputMapel.php?insert=0");
    }
}elseif(isset($_POST['Ubah'])){
    $idMapel = $_POST['id_mapel'];
    $namaMapel = strtoupper($_POST['nama_mapel']);

    $update = ubahMapel($idMapel, $namaMapel);
    if($update>0){
        header("Location: ../viewInputMapel.php?insert=1");
    }else{
        header("Location: ../viewInputMapel.php?insert=0");
    }
}else{
    header("Location: ../404.php");
}
?>