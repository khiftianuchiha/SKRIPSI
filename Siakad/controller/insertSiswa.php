<?php
require_once ('crudSiswa.php');
require_once ('crudPresensi.php');

if(isset($_POST['Tambah'])){
    $sql = "SELECT * from Siswa order by nis DESC LIMIT 1";
    $data = bacaSiswa($sql);
    $id = "0001";
    if ($data != null){
        $id = $data[0]['nis']+1;
        $id = substr($id,3);
        $id = $_POST['id_kelas'].$id;
    }else{
        $id = $_POST['id_kelas'].$id;
    }
    $id_siswa = date("y").$id;
    $nis = str_replace(' ','',$id_siswa);

    $nama_siswa = $_POST['nama'];
    $jk = $_POST['JK'];
    $alamat = $_POST['alamat'];
    $nama_ortu = $_POST['nama_ortu'];
    $alamat_ortu = $_POST['alamat_ortu'];
    $id_ekskul = $_POST['id_ekskul'];
    $password = $_POST['pwd'];
    $id_user = 2; //Siswa
    $id_kelas = $_POST['id_kelas'];
    $ttl = $_POST['tanggal_lahir'];

    //idpresensi
    $sql = "SELECT * from presensi order by id_presensi DESC LIMIT 1";
    $data = bacaPresensi($sql);
    $id_Presensi = 1;
    if ($data != null){
        $id_Presensi = $data[0]['id_presensi']+1;
    }else{
        $id = 1;
    }

    $insert = tambahSiswa($nis, $id_user, $id_kelas, $id_ekskul,$password,$nama_siswa,$jk,$alamat,$ttl,$nama_ortu,$alamat_ortu);
    $insertPresensi = tambahPresensi($id_Presensi, $nis, 0, 0, 0,0);
    if($insert>0){
        header("Location: ../viewSiswa.php?insert=1");
    }else{
        header("Location: ../viewSiswa.php?insert=0");
    }
}elseif(isset($_POST['Batal'])){
    header("Location: ../viewSiswa.php");
}else{
    header("Location: ../404.php");
}
?>