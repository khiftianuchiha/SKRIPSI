<?php 
	require_once ('koneksiDb.php');
	
	function bacaNilai($sql){
		$data = array();
		$koneksi = koneksi();
		$hasil = mysqli_query($koneksi, $sql);
		
		if(mysqli_num_rows($hasil) == 0){
			mysqli_close($koneksi);
			return null;
		}
		
		$i=0;
		while ($baris = mysqli_fetch_array($hasil)){			
			$data[$i]['id_nilai'] = $baris['id_nilai'];
			$data[$i]['id_guru'] = $baris['id_guru'];
			$data[$i]['nis'] = $baris['nis'];
			$data[$i]['id_mapel'] = $baris['id_mapel'];
			$data[$i]['semester'] = $baris['semester'];
			$data[$i]['tahun'] = $baris['tahun'];
			$data[$i]['nilai'] = $baris['nilai'];
			$data[$i]['keterangan'] = $baris['keterangan'];
			$i++;
		}
		mysqli_close($koneksi);
		return $data;
	}
	function bacaSemuaNilai(){
		$sql = "SELECT * from `Nilai`";
		$data = bacaNilai($sql);
		return $data;
	}
	function cariNilai($id_Nilai){
		$sql = "SELECT * from `Nilai` where id_nilai='"+$id_Nilai+"' ";
		$data = bacaNilai($sql);
		return $data;
	}

	function tambahNilai($id_Nilai, $id_guru, $nis, $id_mapel, $semester, $tahun, $nilai, $keterangan){
		$koneksi = koneksi();
		$sql = "INSERT into `Nilai` values($id_Nilai, $id_guru, $nis, $id_mapel, $semester, $tahun, $nilai, '$keterangan')";
		$hasil = 0;
		if(mysqli_query($koneksi, $sql))
			$hasil = 1;
		mysqli_close($koneksi);
		return $hasil;
	}
	function ubahNilai($id_Nilai, $id_guru, $nis, $id_mapel, $semester, $tahun, $nilai, $keterangan){
		$koneksi = koneksi();
		$sql = "UPDATE `nilai` set  id_guru=$id_guru, nis = $nis, id_mapel=$id_mapel, semester = $semester, tahun =$tahun, nilai =$nilai, keterangan = '$keterangan' where id_nilai = $id_Nilai";
		$hasil = 0;
		if(mysqli_query($koneksi, $sql))
			$hasil = 1;
		mysqli_close($koneksi);
		return $hasil;
	}
	
	// menghapus 1 record berdasar field kunci kode
	function hapusNilai($id){
		$koneksi = koneksi();
		$sql = "DELETE from `Nilai` where id_nilai='$id'";
		if (!mysqli_query($koneksi, $sql)){
			die('Error: ' . mysqli_error($koneksi));
		}
		$hasil = mysqli_affected_rows($koneksi);
		mysqli_close($koneksi);
		return $hasil;
	}
?>