<?php 
	require_once ('koneksiDb.php');
	
	function bacaMapel($sql){
		$data = array();
		$koneksi = koneksi();
		$hasil = mysqli_query($koneksi, $sql);
		
		if(mysqli_num_rows($hasil) == 0){
			mysqli_close($koneksi);
			return null;
		}
		
		$i=0;
		while ($baris = mysqli_fetch_array($hasil)){			
			$data[$i]['id_mapel'] = $baris['id_mapel'];
			$data[$i]['nama_mapel'] = $baris['nama_mapel'];
			$i++;
		}
		mysqli_close($koneksi);
		return $data;
	}
	function bacaSemuaMapel(){
		$sql = "SELECT * from Mapel order by id_mapel ";
		$data = bacaMapel($sql);
		return $data;
	}
	function cariMapel($id_Mapel){
		$sql = "SELECT * from Mapel where id_mapel=$id_Mapel";
		$data = bacaMapel($sql);
		return $data;
	}

	function tambahMapel($id_Mapel, $nama_Mapel){
		$koneksi = koneksi();
		$sql = "INSERT into Mapel values($id_Mapel,'$nama_Mapel')";
		$hasil = 0;
		if(mysqli_query($koneksi, $sql))
			$hasil = 1;
		mysqli_close($koneksi);
		return $hasil;
	}

	function ubahMapel($id_Mapel, $nama_Mapel){
		$koneksi = koneksi();
		$sql = "Update Mapel set nama_mapel ='$nama_Mapel' where id_mapel = $id_Mapel";
		$hasil = 0;

		if(mysqli_query($koneksi, $sql))
			$hasil = 1;
		mysqli_close($koneksi);
		return $hasil;
	}
	
	// menghapus 1 record berdasar field kunci kode
	function hapusMapel($id){
		$koneksi = koneksi();
		$sql = "DELETE from Mapel where id_mapel=$id";
		if (!mysqli_query($koneksi, $sql)){
			die('Error: ' . mysqli_error($koneksi));
		}
		$hasil = mysqli_affected_rows($koneksi);
		mysqli_close($koneksi);
		return $hasil;
	}

	function bacaMapelJoinGuru($sql){
		$data = array();
		$koneksi = koneksi();
		$hasil = mysqli_query($koneksi, $sql);
		
		if(mysqli_num_rows($hasil) == 0){
			mysqli_close($koneksi);
			return null;
		}
		
		$i=0;
		while ($baris = mysqli_fetch_array($hasil)){			
			$data[$i]['id_mapel'] = $baris['id_mapel'];
			$data[$i]['nama_mapel'] = $baris['nama_mapel'];
			$data[$i]['id_guru'] = $baris['id_guru'];
			$data[$i]['id_mapel'] = $baris['id_mapel'];
			$data[$i]['id_user'] = $baris['id_user'];
			$data[$i]['nama'] = $baris['nama'];
			$data[$i]['jk'] = $baris['jk'];
			$data[$i]['jabatan'] = $baris['jabatan'];
			$data[$i]['no_hp'] = $baris['no_hp'];
			$data[$i]['password'] = $baris['password'];
			$i++;
		}
		mysqli_close($koneksi);
		return $data;
	}
?>