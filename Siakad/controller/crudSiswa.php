<?php 
	require_once ('koneksiDb.php');
	
	function bacaSiswa($sql){
		$data = array();
		$koneksi = koneksi();
		$hasil = mysqli_query($koneksi, $sql);
		
		if(mysqli_num_rows($hasil) == 0){
			mysqli_close($koneksi);
			return null;
		}
		
		$i=0;
		while ($baris = mysqli_fetch_array($hasil)){			
			$data[$i]['nis'] = $baris['nis'];
			$data[$i]['id_user'] = $baris['id_user'];
			$data[$i]['id_kelas'] = $baris['id_kelas'];
			$data[$i]['id_ekskul'] = $baris['id_ekskul'];
			$data[$i]['password'] = $baris['password'];
			$data[$i]['nama_siswa'] = $baris['nama_siswa'];
			$data[$i]['jk'] = $baris['jk'];
			$data[$i]['alamat'] = $baris['alamat'];
			$data[$i]['ttl'] = $baris['ttl'];
			$data[$i]['nama_ortu'] = $baris['nama_ortu'];
			$data[$i]['alamat_ortu'] = $baris['alamat_ortu'];
			$i++;
		}
		mysqli_close($koneksi);
		return $data;
	}
	function bacaSemuaSiswa(){
		$sql = "SELECT * from Siswa";
		$data = bacaSiswa($sql);
		return $data;
	}
	function cariSiswa($nis){
		$sql = "SELECT * from Siswa where nis=$nis ";
		$data = bacaSiswa($sql);
		return $data;
	}

	function cariSiswaDariKelas($id_kelas){
		$sql = "SELECT * from Siswa where id_kelas=$id_kelas";
		$data = bacaSiswa($sql);
		return $data;
	}

	function tambahSiswa($nis, $id_user, $id_kelas, $id_ekskul,$password,$nama_siswa,$jk,$alamat,$ttl,$nama_ortu,$alamat_ortu){
		$koneksi = koneksi();
		$sql = "INSERT into Siswa values($nis, $id_user, $id_kelas, $id_ekskul,'$password','$nama_siswa','$jk','$alamat','$ttl','$nama_ortu','$alamat_ortu')";
		$hasil = 0;
		if(mysqli_query($koneksi, $sql))
			$hasil = 1;
		mysqli_close($koneksi);
		return $hasil;
	}

	function ubahSiswa($nis, $id_user, $id_kelas, $id_ekskul,$password,$nama_siswa,$jk,$alamat,$ttl,$nama_ortu,$alamat_ortu){
		$koneksi = koneksi();
		$sql = "Update Siswa SET id_kelas = $id_kelas,id_ekskul = $id_ekskul, id_user = $id_user, nama_siswa = '$nama_siswa', jk ='$jk', ttl = '$ttl', alamat ='$alamat', nama_ortu = '$nama_ortu', alamat_ortu = '$alamat_ortu' ,password = '$password' where nis=$nis";

		$hasil = 0;
		if(mysqli_query($koneksi, $sql))
			$hasil = 1;
		mysqli_close($koneksi);
		return $hasil;
	}

	// menghapus 1 record berdasar field kunci kode
	function hapusSiswa($id){
		$koneksi = koneksi();
		$sql = "DELETE from Siswa where nis='$id'";
		if (!mysqli_query($koneksi, $sql)){
			die('Error: ' . mysqli_error($koneksi));
		}
		$hasil = mysqli_affected_rows($koneksi);
		mysqli_close($koneksi);
		return $hasil;
	}

	function bacaSiswaJoinKelas($sql){
		$data = array();
		$koneksi = koneksi();
		$hasil = mysqli_query($koneksi, $sql);
		
		if(mysqli_num_rows($hasil) == 0){
			mysqli_close($koneksi);
			return null;
		}
		
		$i=0;
		while ($baris = mysqli_fetch_array($hasil)){			
			$data[$i]['nis'] = $baris['nis'];
			$data[$i]['id_user'] = $baris['id_user'];
			$data[$i]['id_kelas'] = $baris['id_kelas'];
			$data[$i]['id_ekskul'] = $baris['id_ekskul'];
			$data[$i]['password'] = $baris['password'];
			$data[$i]['nama_siswa'] = $baris['nama_siswa'];
			$data[$i]['jk'] = $baris['jk'];
			$data[$i]['alamat'] = $baris['alamat'];
			$data[$i]['ttl'] = $baris['ttl'];
			$data[$i]['nama_ortu'] = $baris['nama_ortu'];
			$data[$i]['alamat_ortu'] = $baris['alamat_ortu'];
			$data[$i]['id_kelas'] = $baris['id_kelas'];
			$data[$i]['kelas'] = $baris['kelas'];
			$i++;
		}
		mysqli_close($koneksi);
		return $data;
	}

	function bacaSiswaJoinNilaiGuruMapel($sql){
		$data = array();
		$koneksi = koneksi();
		$hasil = mysqli_query($koneksi, $sql);
		
		if(mysqli_num_rows($hasil) == 0){
			mysqli_close($koneksi);
			return null;
		}
		
		$i=0;
		while ($baris = mysqli_fetch_array($hasil)){			
			$data[$i]['nis'] = $baris['nis'];
			$data[$i]['id_user'] = $baris['id_user'];
			$data[$i]['id_kelas'] = $baris['id_kelas'];
			$data[$i]['id_ekskul'] = $baris['id_ekskul'];
			$data[$i]['password'] = $baris['password'];
			$data[$i]['nama_siswa'] = $baris['nama_siswa'];
			$data[$i]['jk'] = $baris['jk'];
			$data[$i]['alamat'] = $baris['alamat'];
			$data[$i]['ttl'] = $baris['ttl'];
			$data[$i]['nama_ortu'] = $baris['nama_ortu'];
			$data[$i]['alamat_ortu'] = $baris['alamat_ortu'];
			$data[$i]['id_nilai'] = $baris['id_nilai'];
			$data[$i]['id_guru'] = $baris['id_guru'];
			$data[$i]['nis'] = $baris['nis'];
			$data[$i]['id_mapel'] = $baris['id_mapel'];
			$data[$i]['semester'] = $baris['semester'];
			$data[$i]['tahun'] = $baris['tahun'];
			$data[$i]['nilai'] = $baris['nilai'];
			$data[$i]['keterangan'] = $baris['keterangan'];
			$data[$i]['id_guru'] = $baris['id_guru'];
			$data[$i]['id_mapel'] = $baris['id_mapel'];
			$data[$i]['id_user'] = $baris['id_user'];
			$data[$i]['nama'] = $baris['nama'];
			$data[$i]['jk'] = $baris['jk'];
			$data[$i]['jabatan'] = $baris['jabatan'];
			$data[$i]['no_hp'] = $baris['no_hp'];
			$data[$i]['password'] = $baris['password'];
			$data[$i]['id_mapel'] = $baris['id_mapel'];
			$data[$i]['nama_mapel'] = $baris['nama_mapel'];
			$i++;
		}
		mysqli_close($koneksi);
		return $data;
	}
?>