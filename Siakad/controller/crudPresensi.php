<?php 
	require_once ('koneksiDb.php');
	
	function bacaPresensi($sql){
		$data = array();
		$koneksi = koneksi();
		$hasil = mysqli_query($koneksi, $sql);
		
		if(mysqli_num_rows($hasil) == 0){
			mysqli_close($koneksi);
			return null;
		}
		
		$i=0;
		while ($baris = mysqli_fetch_array($hasil)){			
			$data[$i]['id_presensi'] = $baris['id_presensi'];
			$data[$i]['nis'] = $baris['nis'];
			$data[$i]['hadir'] = $baris['hadir'];
			$data[$i]['ijin'] = $baris['ijin'];
			$data[$i]['sakit'] = $baris['sakit'];
			$data[$i]['alpha'] = $baris['alpha'];
			$i++;
		}
		mysqli_close($koneksi);
		return $data;
	}
	function bacaSemuaPresensi(){
		$sql = "SELECT * from Presensi";
		$data = bacaPresensi($sql);
		return $data;
	}
	function cariPresensi($id_Presensi){
		$sql = "SELECT * from Presensi where id_presensi=$id_Presensi ";
		$data = bacaPresensi($sql);
		return $data;
	}

	function tambahPresensi($id_Presensi, $nis, $hadir, $ijin, $sakit,$alpha){
		$koneksi = koneksi();
		$sql = "INSERT into Presensi values($id_Presensi, $nis, $hadir, $ijin, $sakit,$alpha)";
		$hasil = 0;
		if(mysqli_query($koneksi, $sql))
			$hasil = 1;
		mysqli_close($koneksi);
		return $hasil;
	}

	function ubahPresensi($id_Presensi, $nis, $hadir, $ijin, $sakit,$alpha){
		$koneksi = koneksi();
		$sql = "UPDATE Presensi set nis= $nis, hadir=$hadir,ijin= $ijin,sakit= $sakit,alpha=$alpha where id_presensi=$id_Presensi";
		$hasil = 0;
		if(mysqli_query($koneksi, $sql))
			$hasil = 1;
		mysqli_close($koneksi);
		return $hasil;
	}
	
	// menghapus 1 record berdasar field kunci kode
	function hapusPresensi($id){
		$koneksi = koneksi();
		$sql = "DELETE from Presensi where id_presensi='$id'";
		if (!mysqli_query($koneksi, $sql)){
			die('Error: ' . mysqli_error($koneksi));
		}
		$hasil = mysqli_affected_rows($koneksi);
		mysqli_close($koneksi);
		return $hasil;
	}

	function bacaPresensiJoinSiswa($sql){
		$data = array();
		$koneksi = koneksi();
		$hasil = mysqli_query($koneksi, $sql);
		
		if(mysqli_num_rows($hasil) == 0){
			mysqli_close($koneksi);
			return null;
		}
		
		$i=0;
		while ($baris = mysqli_fetch_array($hasil)){			
			$data[$i]['id_presensi'] = $baris['id_presensi'];
			$data[$i]['nis'] = $baris['nis'];
			$data[$i]['hadir'] = $baris['hadir'];
			$data[$i]['ijin'] = $baris['ijin'];
			$data[$i]['sakit'] = $baris['sakit'];
			$data[$i]['alpha'] = $baris['alpha'];
			$data[$i]['nis'] = $baris['nis'];
			$data[$i]['id_user'] = $baris['id_user'];
			$data[$i]['id_kelas'] = $baris['id_kelas'];
			$data[$i]['id_ekskul'] = $baris['id_ekskul'];
			$data[$i]['password'] = $baris['password'];
			$data[$i]['nama_siswa'] = $baris['nama_siswa'];
			$data[$i]['jk'] = $baris['jk'];
			$data[$i]['alamat'] = $baris['alamat'];
			$data[$i]['ttl'] = $baris['ttl'];
			$data[$i]['nama_ortu'] = $baris['nama_ortu'];
			$data[$i]['alamat_ortu'] = $baris['alamat_ortu'];
			$data[$i]['id_kelas'] = $baris['id_kelas'];
			$data[$i]['kelas'] = $baris['kelas'];
			$i++;
		}
		mysqli_close($koneksi);
		return $data;
	}
?>