<?php
require_once ('crudKelas.php');

if(isset($_POST['Tambah'])){
    $idKelas = $_POST['id_kelas'];
    $namaKelas = strtoupper($_POST['nama_kelas']);

    $insert = tambahKelas($idKelas, $namaKelas);
    if($insert>0){
        header("Location: ../viewInputKelas.php?insert=1");
    }else{
        header("Location: ../viewInputKelas.php?insert=0");
    }
}elseif(isset($_POST['Ubah'])){
    $idKelas = $_POST['id_kelas'];
    $namaKelas = strtoupper($_POST['nama_kelas']);

    $update = ubahKelas($idKelas, $namaKelas);
    if($update>0){
        header("Location: ../viewInputKelas.php?insert=1");
    }else{
        header("Location: ../viewInputKelas.php?insert=0");
    }
}else{
    header("Location: ../404.php");
}
?>