<?php include 'header.php' ?>


    <div class="container">
    <img class="img-fluid" src="image/mathematics-757566_1920.jpg" alt="">

        <div class="row">
          <div class="col-md-6" style="margin-top:2%">
            <div class="card">
              <div class="card-body">
                <h3 class="card-title">Soal</h3>
                <p class="card-text">Ujung sebuah tali yang panjangnya 1 meter di getarkan sehingga dalam waktu 2 sekon terdapat 2 gelombang. tentukanlah persamaan gelombang tersebut apabila amplitudo getaran ujung tali 20 cm.</p>
              </div>
              <a class="btn btn-info btn-lg" href="">Bantu Jawab</a>
            </div>
          </div>
          <div class="col-md-6" style="margin-top:2%">
            <div class="card">
              <div class="card-body">
                <h3 class="card-title">Soal</h3>
                <p class="card-text">Suatu getaran yang frekuensinya 30 Hz. Jika jarak antara puncak dan lembah gelombang yang berturutan adalah 50 cm, hitunglah cepat rambat gelombang tersebut!</p>
              </div>
              <a class="btn btn-info btn-lg" href="">Bantu Jawab</a>
            </div>
          </div>
          <div class="col-md-6" style="margin-top:2%">
            <div class="card">
              <div class="card-body">
                <h3 class="card-title">Soal</h3>
                <p class="card-text">Sebuah pemancar radio bekerja pada gelombang 1,5 m. Jika cepat rambat gelombang radio 3.108 m/s, pada frekuensi berapakah stasion radio tersebut bekerja!</p>
              </div>
              <a class="btn btn-info btn-lg" href="">Bantu Jawab</a>
            </div>
          </div>
          <div class="col-md-6" style="margin-top:2%">
            <div class="card">
              <div class="card-body">
                <h3 class="card-title">Soal</h3>
                <p class="card-text">Gelombang berjalan mempunyai persmaan y = 0,2 sin (100π t – 2π x). Tentukan amplitudo, periode, frekuensi, panjang gelombang, dan cepat rambat gelombang tersebut !</p>
              </div>
              <a class="btn btn-info btn-lg" href="">Bantu Jawab</a>
            </div>
          </div>
        </div>

        <nav aria-label="Page navigation" style="margin-top : 3%">
          <ul class="pagination ">
            <li class="page-item disabled">
              <a class="page-link" href="#" aria-label="Previous">
                <span aria-hidden="true">&laquo;</span>
                <span class="sr-only">Previous</span>
              </a>
            </li>
            <li class="page-item active"><a class="page-link" href="#">1</a></li>
            <li class="page-item"><a class="page-link" href="#">2</a></li>
            <li class="page-item"><a class="page-link" href="#">3</a></li>
            <li class="page-item"><a class="page-link" href="#">4</a></li>
            <li class="page-item">
              <a class="page-link" href="#" aria-label="Next">
                <span aria-hidden="true">&raquo;</span>
                <span class="sr-only">Next</span>
              </a>
            </li>
          </ul>
        </nav>

    </div>


<?php include 'footer.php' ?>